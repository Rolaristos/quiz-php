# Projet Light Novel Quizz

Contexte : Nous avons basés le projet sur une histoire nommée Minami no hanashi que nous avons écris de toute pièce. Nous envisageons sur le long terme de faire de cette base, un véritable projet personnel.

## Équipe de Développement

* Rebson Dodji DAHOUEDE
* Roland RAKOTOMALALA

## Fichier de Connexion à la Base de Données

Si vous rencontrez des difficultés à cause de la connexion à la base de donnée, vous pouvez simplement le modifier en y modifiant les identifiants et la base de donnée.

Le fichier se trouve à l'emplacement suivant : `./db/php/DB_connection.php`

## Scripts de Création de la Base de Données et d'Insertions

Les scripts nécessaires à la création de la base de données et aux insertions se trouvent dans les emplacements suivants :

* Création de la base de données + suppressions : `./db/mysql/CREATION.sql`
* Insertions : `./db/mysql/INSERTION.sql`

  À noter que dans le fichier `./db/mysql/CREATION.sql`, cette ligne a été mise en commentaire : CREATE DATABASE IF NOT EXISTS MINAMI_NO_BD;

  Elle n'était utile que dans nos pc locaux, mais ne le sera pas (normalement) dans les pc de l'IUT car nous n'avons pas les droits nécessaire pour pouvoir créer une database. DB+nom de famille étant celle utilisée de base.

## Lancement du projet - Informations

La commande suivante est à exécuter à la racine du projet (donc dans le dossier `web`) :

`php -S localhost:8000`

Veuillez à bien charger dans votre base de donnée le script de création ainsi que les fichiers d'insertions

Pour voir et essayer le côté **Admin**, il va vous falloir entrer les informations suivantes dans les champs de connexions :

**Pseudo/Username** : adm

**Mot de passe** : adm

Cela va immédiatement vous rediriger vers la page qui doit vous permettre de gérer différents élément du Quiz.

Sinon, il va vous suffire de créer un compte, puis d'aller dans la section 'Quizz' et de commencer le jeu.

## Informations concernant le projet

- Nous n'avons pas réussi à nous servir du Autoloader. Même avec l'aide de diverse forum et de la doc, nous ne sommes pas parvenu à le mettre en place à ce jour. Nous avons tout de même choisi de le garder, mais avions quand même fait le nécessaire pour que tout fonctionne correctement.
- L'insertion de nouvelles questions est encore en phase de développement. Nous essayons à ce jour, encore de forcer l'administrateur a bien cocher au moins une réponse valide pour une question. Sans succès aujourd'hui mais toujours en cours de développement et d'améliorations.

## Informations sur l'arborescence

Comme nous voulions faire de ce projet, un projet concret et personnel, nous avons pour objectif de créer un site avec plusieurs rubrique.  
Light novel étant la première rubrique (représentant l'accueil) et quiz une autre, nous avons fait un dossier pour ces 2 différentes rubriques.
Ce qui explique l'existence du dossier `light_novel`.  
Pour le dossier admin, dans une perspective plus lointaine, devra être en mesure de gérer l'ensemble des fonctionalités du site. C'est pourquoi il a été placé en dehors des autres dossiers.

## Documentation Additionnelle

* Le fichier `next-features.txt` dans le répertoire `light_novel` contient les fonctionnalités et ajustements prévues pour les prochaines versions du projet.
* Le fichier `backsave.php` dans le répertoire `Quizz` représente uniquement la base à partir de laquelle nous sommes partis (base vue en cours, donc pas besoin de le consulter).
