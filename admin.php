<?php
    session_start();
    require_once __DIR__ . '/Quizz/Classes/Autoloader.php';

    use \Quizz\Classes\Autoloader;

    Autoloader::register();

    require_once __DIR__ . '/Quizz/Classes/Question/QuizHandler.php';
    require_once __DIR__ . '/db/php/DB_connection.php';
    require_once __DIR__ . '/db/php/DBClasses/QuestionDB.php';
    require_once __DIR__ . '/db/php/DBClasses/QuizzDB.php';
    require_once __DIR__ . '/db/php/DBClasses/QuizzTermineDB.php';

    use db\php\DBClasses\QuizzTermineDB;
    use db\php\DBClasses\QuestionDB;
    use db\php\DBClasses\QuizzDB;

    $serializedUser = $_SESSION['user'];
    $loggedInUser = unserialize($serializedUser);

    $pseudo = $loggedInUser->getPseudo();

    // Vérification si l'utilisateur est connecté en tant qu'administrateur
    if(isset($_SESSION['user']) && $pseudo == "adm") {
    } else {
        header("Location: /light_novel/templates/login.php");
        exit;
    }
    
    $quizzDB = new QuizzDB($cnx);
    $quizzes = $quizzDB->getAllQuizz();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Tableau de bord</title>
    <!-- Le style ne prenant pas beaucoup de lignes, nous avons décidés de le laisser ici -->
    <style>
        body {
            background-color: #202020;
            color: #fff;
            font-family: 'Arial', sans-serif;
        }

        form {
            margin: 20px 0;
        }

        label {
            display: block;
            margin-bottom: 5px;
        }

        input, textarea, select {
            width: 100%;
            padding: 8px;
            margin-bottom: 15px;
            box-sizing: border-box;
        }

        input[type="submit"] {
            background-color: #4CAF50;
            color: #fff;
            cursor: pointer;
        }

        h2 {
            color: #4CAF50;
        }

        ul {
            list-style: none;
            padding: 0;
        }

        li {
            margin-bottom: 10px;
        }

        a {
            color: #3498db;
            text-decoration: none;
            margin-left: 10px;
        }

        a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <a href="/light_novel/templates/minami_no_web.php">Accueil</a>
    <!-- Formulaire pour la création de quiz -->
    <form method="POST" action="/admin/request/quiz_creation.php">
        <label>Nom du Quiz (max. 50 caractères):</label>
        <input type="text" name="quizName" maxlength="50" required>

        <label>Description du Quiz (max. 255 caractères):</label>
        <textarea name="quizDescription" maxlength="255" required></textarea>

        <label>Difficulté du Quiz:</label>
        <select name="quizDifficulty" required>
            <option value="1">Facile</option>
            <option value="2">Normal</option>
            <option value="3">Difficile</option>
            <option value="4">Extrême</option>
        </select>

        <input type="submit" name="createQuiz" value="Passer à la création des questions">
    </form>

    <!-- Liste des quiz existants -->
    <h2>Liste des Quiz</h2>
    <ul>
        <?php
        foreach ($quizzes as $quiz) {
            echo "<li>" . $quiz->getName() . " - <a href='/admin/request/delete_quiz.php?action=deleteQuiz&id=" . $quiz->getId() . "'>Supprimer</a> - <a href='/admin/templates/viewQuestions.php?action=viewQuestions&id=" . $quiz->getId() . "'>Voir Questions</a></li>";
        }
        ?>
    </ul>

</body>
</html>
