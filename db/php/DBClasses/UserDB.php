<?php
namespace db\php\DBClasses;

require_once __DIR__ . '/../../../Quizz/Classes/Autoloader.php';
use \Quizz\Classes\Autoloader;

Autoloader::register();

use PDO;
use Quizz\Classes\User\User;

class UserDB
{
    private PDO $conn;

    /**
     * Constructeur de la classe UserDB.
     *
     * @param PDO $conn Une instance de la classe PDO représentant la connexion à la base de données.
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Récupère un utilisateur par son pseudo dans la base de données.
     *
     * @param string $pseudo Le pseudo de l'utilisateur à rechercher.
     * @return User|null Un objet User si l'utilisateur est trouvé, sinon null.
     */
    public function getUserByPseudo(string $pseudo): ?User
    {
        $query = "SELECT * FROM USER WHERE pseudo = :pseudo";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        $stmt->execute();
    
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        if ($row !== false) {
            return $this->mapToUser($row);
        } else {
            return null;
        }
    }

    /**
     * Ajoute un nouvel utilisateur à la base de données.
     *
     * @param User $user L'objet User à ajouter.
     * @return bool True si l'opération est réussie, sinon false.
     */
    public function addUser(User $user): bool
    {
        $query = "INSERT INTO USER (pseudo, mdp) VALUES (:pseudo, :mdp)";
        $stmt = $this->conn->prepare($query);
    
        $pseudo = $user->getPseudo();
        $mdp = password_hash($user->getMdp(), PASSWORD_DEFAULT);
    
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        $stmt->bindParam(":mdp", $mdp, PDO::PARAM_STR);
        
        return $stmt->execute();
    }

    /**
     * Supprime un utilisateur de la base de données en fonction de son pseudo.
     *
     * @param string $pseudo Le pseudo de l'utilisateur à supprimer.
     * @return bool True si l'opération est réussie, sinon false.
     */
    public function deleteUser(string $pseudo): bool
    {
        $query = "DELETE FROM USER WHERE pseudo = :pseudo";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        return $stmt->execute();
    }

    /**
     * Vérifie si un utilisateur existe déjà dans la base de données en fonction de son pseudo.
     *
     * @param string $pseudo Le pseudo de l'utilisateur à vérifier.
     * @return bool True si l'utilisateur existe, sinon false.
     */
    public function userExists(string $pseudo): bool
    {
        $query = "SELECT COUNT(*) as count FROM USER WHERE pseudo = :pseudo";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result['count'] > 0;
    }

    /**
     * Transforme un tableau de données de base de données en un objet User.
     *
     * @param array $row Le tableau de données de la base de données.
     * @return User Un objet User créé à partir des données.
     */
    private function mapToUser($row): User
    {
        $pseudo = $row['pseudo'];
        $mdp = $row['mdp'];
        return new User($pseudo, $mdp);
    }

    /**
     * Vérifie si le mot de passe fourni correspond au mot de passe stocké en base de données.
     *
     * @param string $pseudo Le pseudo de l'utilisateur.
     * @param string $password Le mot de passe à vérifier.
     * @return bool True si les mots de passe correspondent, sinon false.
     */
    public function checkPassword(string $pseudo, string $password): bool
    {
        $query = "SELECT mdp FROM USER WHERE pseudo = :pseudo";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result !== false) {
            $hashedPasswordFromDB = $result['mdp'];
            return password_verify($password, $hashedPasswordFromDB);
        } else {
            return false;
        }
    }    
}
?>
