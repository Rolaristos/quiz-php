<?php
namespace db\php\DBClasses;

require_once __DIR__ . '/../../../Quizz/Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

use PDO;
use Quizz\Classes\QuizzTermine\QuizzTermine;

class QuizzTermineDB
{
    private PDO $conn;

    /**
     * Constructeur de la classe QuizzTermineDB.
     *
     * @param PDO $conn Une instance de la classe PDO représentant la connexion à la base de données.
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Récupère tous les quizz terminés dans la base de données.
     *
     * @return array Un tableau d'objets QuizzTermine.
     */
    public function getAllQuizzTermine(): array
    {
        $quizzTermineArray = array();

        $query = "SELECT * FROM QUIZZ_TERMINE";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $quizzTermine = $this->mapToQuizzTermine($row);
            $quizzTermineArray[] = $quizzTermine;
        }

        return $quizzTermineArray;
    }

    /**
     * Récupère tous les quizz terminés par un utilisateur spécifique.
     *
     * @param string $pseudo Le pseudo de l'utilisateur.
     * @return array Un tableau d'objets QuizzTermine.
     */
    public function getAllQuizzTermineByPseudo(string $pseudo): array
    {
        $quizzTermineArray = array();

        $query = "SELECT * FROM QUIZZ_TERMINE WHERE pseudo = :pseudo";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $quizzTermine = $this->mapToQuizzTermine($row);
            $quizzTermineArray[] = $quizzTermine;
        }

        return $quizzTermineArray;
    }

    /**
     * Récupère tous les quizz terminés avec les détails du quizz pour un utilisateur spécifique.
     *
     * @param string $pseudo Le pseudo de l'utilisateur.
     * @return array Un tableau d'objets Quizz.
     */
    public function getAllQuizzFini(string $pseudo): array
    {
        $quizzTermineArray = array();

        $query = "SELECT quizz_id, quizz_name, quizz_description, quizz_difficulte FROM QUIZZ_TERMINE NATURAL JOIN QUIZZ WHERE pseudo = :pseudo";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $__QUIZZ_DB__ = new QuizzDB($this->conn);
            $quizzTermine = $__QUIZZ_DB__->mapToQuizz($row);
            $quizzTermineArray[] = $quizzTermine;
        }

        return $quizzTermineArray;
    }

    /**
     * Récupère le score d'un utilisateur pour un quizz spécifique.
     *
     * @param string $pseudo Le pseudo de l'utilisateur.
     * @param int $quizzId L'ID du quizz.
     * @return int|null Le score de l'utilisateur pour le quizz ou null si non trouvé.
     */
    public function getScoreByPseudoAndQuizzId(string $pseudo, int $quizzId): ?int
    {
        $query = "SELECT score FROM QUIZZ_TERMINE WHERE pseudo = :pseudo AND quizz_id = :quizzId";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        $stmt->bindParam(":quizzId", $quizzId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($result)) {
            return (int)$result[0]['score'];
        } else {
            return null;
        }
    }

    /**
     * Récupère le pseudo de l'utilisateur et son score pour le quizz avec le score maximal.
     *
     * @param int $quizz_id L'ID du quizz.
     * @return array|null Un tableau avec 'pseudo' et 'max_score' ou null si non trouvé.
     */
    public function getMaxScore(int $quizz_id): ?array
    {
        $query = "SELECT pseudo, score AS max_score
        FROM QUIZZ_TERMINE
        WHERE quizz_id = :quizzId
        AND score = (SELECT MAX(score) FROM QUIZZ_TERMINE WHERE quizz_id = :quizzId);
        ";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":quizzId", $quizz_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($result)) {
            return [
                'max_score' => (int)$result[0]['max_score'],
                'pseudo' => $result[0]['pseudo']
            ];
        } else {
            return null;
        }
    }

    /**
     * Transforme un tableau de données de base de données en un objet QuizzTermine.
     *
     * @param array $row Le tableau de données de la base de données.
     * @return QuizzTermine Un objet QuizzTermine créé à partir des données.
     */
    private function mapToQuizzTermine($row): QuizzTermine
    {
        $pseudo = $row['pseudo'];
        $quizzId = (int)$row['quizz_id'];
        $score = (int)$row['score'];
        return new QuizzTermine($pseudo, $quizzId, $score);
    }

    /**
     * Insère un enregistrement de quizz terminé dans la base de données.
     *
     * @param string $pseudo Le pseudo de l'utilisateur.
     * @param int $quizzId L'ID du quizz.
     * @param int $score Le score de l'utilisateur pour le quizz.
     * @return bool True si l'opération est réussie, sinon false.
     */
    public function insertQuizzTermine(string $pseudo, int $quizzId, int $score): bool
    {
        $query = "INSERT INTO QUIZZ_TERMINE (pseudo, quizz_id, score) VALUES (:pseudo, :quizzId, :score)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":pseudo", $pseudo, PDO::PARAM_STR);
        $stmt->bindParam(":quizzId", $quizzId, PDO::PARAM_INT);
        $stmt->bindParam(":score", $score, PDO::PARAM_INT);

        return $stmt->execute();
    }
}
?>
