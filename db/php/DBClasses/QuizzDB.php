<?php
namespace db\php\DBClasses;

require_once __DIR__ . '/../../../Quizz/Classes/Autoloader.php';
use \Quizz\Classes\Autoloader;

Autoloader::register();
require_once __DIR__ . '/../DB_connection.php';
require_once __DIR__ . '/QuestionDB.php';

use PDO;
use Quizz\Classes\Quizz;

class QuizzDB
{
    private $conn;

    /**
     * Constructeur de la classe QuizzDB.
     *
     * @param PDO $conn Une instance de la classe PDO représentant la connexion à la base de données.
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Récupère tous les quizz depuis la base de données.
     *
     * @return array Un tableau d'objets Quizz.
     */
    public function getAllQuizz(): array
    {
        $quizzArray = array();

        $query = "SELECT * FROM QUIZZ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $quizz = $this->mapToQuizz($row);
            $quizzArray[] = $quizz;
        }

        return $quizzArray;
    }

    /**
     * Ajoute les questions associées à un quizz à partir de la base de données.
     *
     * @param int $quizz_id L'ID du quizz.
     * @return array Un tableau d'objets Question.
     */
    public function addQuestions($quizz_id): array
    {
        $DBQuestion = new QuestionDB($this->conn);
        $questions = [];
        $query = "SELECT question_id FROM QUESTION_QUIZZ WHERE quizz_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $quizz_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $questions[] = $DBQuestion->getQuestionByID((int) $row['question_id']);
        }

        return $questions;
    }

    /**
     * Transforme un tableau de données de base de données en un objet Quizz.
     *
     * @param array $row Le tableau de données de la base de données.
     * @return Quizz Un objet Quizz créé à partir des données.
     */
    public function mapToQuizz($row): Quizz
    {
        $questions = $this->addQuestions($row['quizz_id']);
        $quizz = new Quizz((int) $row['quizz_id'], $row['quizz_name'], $questions, $row['quizz_description'], (int) $row['quizz_difficulte']);
        return $quizz;
    }

    /**
     * Crée un nouveau quiz dans la base de données.
     *
     * @param string $quizName Le nom du quiz.
     * @param string $quizDescription La description du quiz.
     * @param int $quizDifficulty La difficulté du quiz.
     * @return bool True si l'opération est réussie, sinon false.
     */
    public function createQuiz(string $quizName, string $quizDescription, int $quizDifficulty): bool
    {
        $query = "INSERT INTO QUIZZ (quizz_name, quizz_description, quizz_difficulte) VALUES (?, ?, ?)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $quizName, PDO::PARAM_STR);
        $stmt->bindParam(2, $quizDescription, PDO::PARAM_STR);
        $stmt->bindParam(3, $quizDifficulty, PDO::PARAM_INT);

        $success = $stmt->execute();

        $stmt=null;

        return $success;
    }

    /**
     * Supprime un quiz de la base de données en fonction de son ID.
     *
     * @param int $quizId L'ID du quiz à supprimer.
     * @return bool True si l'opération est réussie, sinon false.
     */
    public function deleteQuizById(int $quizId): bool
    {
        // Supprimer d'abord les liaisons dans la table QUESTION_QUIZZ
        $this->deleteQuestionsForQuiz($quizId);

        // Supprimer les scores associés dans la table QUIZZ_TERMINE
        $this->deleteScoresForQuiz($quizId);

        $query = "DELETE FROM QUESTION WHERE question_id IN (SELECT question_id FROM QUESTION_QUIZZ WHERE quizz_id = ?)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $quizId, PDO::PARAM_INT);
        $stmt->execute();
        $stmt=null;

        // Ensuite, supprimer le quiz de la table QUIZZ
        $query = "DELETE FROM QUIZZ WHERE quizz_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $quizId, PDO::PARAM_INT);
        $success = $stmt->execute();
        $stmt=null;

        return $success;
    }

    /**
     * Supprime les questions associées à un quiz de la base de données.
     *
     * @param int $quizId L'ID du quiz.
     */
    private function deleteQuestionsForQuiz(int $quizId): void
    {
        // Récupérer les questions associées au quiz
        $questions = $this->addQuestions($quizId);

        // Supprimer les liaisons dans la table QUESTION_QUIZZ
        $query = "DELETE FROM QUESTION_QUIZZ WHERE quizz_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $quizId, PDO::PARAM_INT);
        $stmt->execute();
        $stmt=null;

        // Supprimer les réponses associées dans la table ANSWER
        foreach ($questions as $question) {
            if ($question instanceof Question) {
                $this->deleteAnswersForQuestion($question->getId());
            }
        }
    }

    /**
     * Supprime les réponses associées à une question de la base de données.
     *
     * @param int $questionId L'ID de la question.
     */
    private function deleteAnswersForQuestion(int $questionId): void
    {
        // Supprimer les réponses associées dans la table ANSWER
        $query = "DELETE FROM ANSWER WHERE question_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $questionId, PDO::PARAM_INT);
        $stmt->execute();
        $stmt=null;
    }

    /**
     * Supprime les scores associés à un quiz de la base de données.
     *
     * @param int $quizId L'ID du quiz.
     */
    private function deleteScoresForQuiz(int $quizId): void
    {
        // Supprimer les scores associés dans la table QUIZZ_TERMINE
        $query = "DELETE FROM QUIZZ_TERMINE WHERE quizz_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $quizId, PDO::PARAM_INT);
        $stmt->execute();
        $stmt=null;
    }

    /**
     * Récupère les quizz en fonction de leur difficulté depuis la base de données.
     *
     * @param int $level La difficulté du quizz.
     * @return array Un tableau d'objets Quizz.
     */
    public function getQuizzByDifficult(int $level): array
    {
        $query = "SELECT * FROM QUIZZ WHERE quizz_difficulte = ?";
        $quizzArray = [];
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $level, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $quizzArray[] = $this->mapToQuizz($row);
        }

        return $quizzArray;
    }

    /**
     * Récupère un quizz en fonction de son ID depuis la base de données.
     *
     * @param int $id L'ID du quizz.
     * @return Quizz|null Un objet Quizz ou null si non trouvé.
     */
    public function getQuizzById(int $id): Quizz
    {
        $query = "SELECT * FROM QUIZZ WHERE quizz_id = ?";
        $quizz = null;
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $quizz = $this->mapToQuizz($row);
        }

        return $quizz;
    }

    /**
     * Récupère une liste aléatoire de quizz depuis la base de données.
     *
     * @param int $limit Le nombre limite de quizz à récupérer (par défaut à 50).
     * @return array Un tableau d'objets Quizz.
     */
    public function getRandomQuizzList(int $limit = 50): array
    {
        $query = "SELECT * FROM QUIZZ ORDER BY RAND() LIMIT ?";
        $quizzArray = [];
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $limit, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($result as $row) {
            $quizz = $this->mapToQuizz($row);
            $quizzArray[] = $quizz;
        }

        return $quizzArray;
    }

    /**
     * Récupère le numéro de chapitre d'un quizz par son ID.
     *
     * @param int $id L'ID du quizz.
     * @return int Le numéro de chapitre du quizz ou -1 s'il n'est pas trouvé.
     */
    public function getChapterById(int $id): int
    {
        $query = "SELECT chapter_number FROM QUIZZ WHERE quizz_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $chapter_id = -1;

        foreach ($result as $row) {
            $chapter_id = $row['chapter_number'] !== null ? (int) $row['chapter_number'] : -1;
        }

        return $chapter_id;
    }

    /**
     * Récupère le score d'un utilisateur sur un quizz spécifique.
     *
     * @param string $pseudo Le pseudo de l'utilisateur.
     * @param int $quiz_id L'ID du quizz.
     * @return int Le score de l'utilisateur sur le quizz, 0 s'il n'y a pas de score trouvé.
     */
    public function getScoreOnQuizz(string $pseudo, int $quiz_id)
    {
        $query = "SELECT score FROM QUIZZ_TERMINE WHERE pseudo = ? AND quizz_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $pseudo, PDO::PARAM_STR);
        $stmt->bindParam(2, $quiz_id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $score = 0;

        foreach ($result as $row) {
            $score = $row['score'];
        }

        return $score;
    }

    /**
     * Récupère l'ID du dernier quizz enregistré.
     *
     * @return int|null L'ID du dernier quizz ou null s'il n'y a pas de quizz enregistré.
     */
    public function getDernierQuiz(): ?int
    {
        $query = "SELECT MAX(quizz_id) AS max_id FROM QUIZZ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
    
        if ($result !== false) {
            return $result['max_id'];
        }
    
        return null;
    }

}
?>