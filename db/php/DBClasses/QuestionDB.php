<?php

namespace db\php\DBClasses;

require_once __DIR__ . '/../../../Quizz/Classes/Autoloader.php';
require_once __DIR__ . '/../../../Quizz/Classes/Question/Question.php';
require_once __DIR__ . '/../../../Quizz/Classes/Question/QuestionRadio.php';
require_once __DIR__ . '/../../../Quizz/Classes/Question/QuestionCheckbox.php';
require_once __DIR__ . '/../../../Quizz/Classes/Question/QuestionText.php';
require_once __DIR__ . '/../../../Quizz/Classes/Answer/Answer.php';
require_once __DIR__ . '/../DB_connection.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

use PDO;
use Quizz\Classes\Question\Question;
use Quizz\Classes\Question\QuestionRadio;
use Quizz\Classes\Question\QuestionCheckbox;
use Quizz\Classes\Question\QuestionText;
use Quizz\Classes\Answer\Answer;
use Quizz\Classes\Choice\Choice;

class QuestionDB
{
    private PDO $conn;

    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * Récupère toutes les questions de la base de données.
     *
     * @return array Un tableau d'objets Question.
     */
    public function getQuestions(): array
    {
        $questions = [];

        $query = "SELECT * FROM QUESTION";
        $stmt = $this->conn->query($query);

        if ($stmt !== false) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $question = $this->createQuestion($row);
                $questions[] = $question;
            }
        }

        return $questions;
    }

    /**
     * Crée un objet Question à partir des données de la base de données.
     *
     * @param array $data Les données de la question.
     * @return Question Un objet Question.
     * @throws \Exception Si le type de question n'est pas pris en charge.
     */
    private function createQuestion(array $data): Question
    {
        $name = $data['question_id'];
        $text = $data['question_text'];
        $type = $data['type'];
        $score = $data['score'];

        $answers = $this->getAnswersForQuestion($name);

        switch ($type) {
            case 'radio':
                $choices = $this->getChoicesForQuestion($name);
                return new QuestionRadio($name, $type, $text, $answers, $score, $choices);

            case 'checkbox':
                $choices = $this->getChoicesForQuestion($name);
                return new QuestionCheckbox($name, $type, $text, $answers, $score, $choices);

            case 'text':
                return new QuestionText($name, $type, $text, $answers, $score);

            default:
                throw new \Exception("Type de question non pris en charge : $type");
        }
    }

    /**
     * Récupère les réponses valides pour une question donnée.
     *
     * @param int $questionId L'ID de la question.
     * @return array Un tableau d'objets Answer.
     */
    private function getAnswersForQuestion(int $questionId): array
    {
        $query = "SELECT * FROM ANSWER WHERE question_id = ? AND est_valide = TRUE";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $questionId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $answers = [];
        foreach ($result as $row) {
            $answers[] = new Answer($row['answer_id'], $row['answer_text'], $row['question_id'], $row['est_valide']);
        }

        return $answers;
    }

    /**
     * Récupère les choix associés à une question donnée.
     *
     * @param int $questionId L'ID de la question.
     * @return array Un tableau d'objets Choice.
     */
    private function getChoicesForQuestion(int $questionId): array
    {
        $query = "SELECT * FROM ANSWER WHERE question_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $questionId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $choices = [];
        foreach ($result as $row) {
            $choices[] = new Choice($row['answer_text'], $row['answer_id']);
        }

        return $choices;
    }

    /**
     * Récupère une question par son ID.
     *
     * @param int $questionId L'ID de la question.
     * @return Question Un objet Question.
     * @throws \Exception Si la question avec l'ID donné n'existe pas.
     */
    public function getQuestionByID(int $questionId): Question
    {
        $query = "SELECT * FROM QUESTION WHERE question_id = ?";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $questionId, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (count($result) > 0) {
            return $this->createQuestion($result[0]);
        }

        throw new \Exception("La question avec l'identifiant " . (string) $questionId . " n'existe pas.");
    }

    /**
     * Génère un tableau de questions aléatoires.
     *
     * @return array Un tableau d'objets Question.
     */
    public function generateRandomQuestions(): array
    {
        $allQuestions = $this->getQuestions();
        $totalQuestions = count($allQuestions);

        // Générer un nombre aléatoire de questions
        $numQuestions = rand(1, $totalQuestions);

        $randomQuestions = [];
        $randomIndexes = array_rand($allQuestions, $numQuestions);

        foreach ($randomIndexes as $index) {
            $randomQuestions[] = $allQuestions[$index];
        }

        return $randomQuestions;
    }

    /**
     * Récupère l'ID de la dernière question enregistrée.
     *
     * @return int|null L'ID de la dernière question ou null s'il n'y a pas de question enregistrée.
     */
    public function getDerniereQuestionId(): ?int
    {
        $query = "SELECT MAX(question_id) AS max_id FROM QUESTION";
        $stmt = $this->conn->query($query);

        if ($stmt !== false) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['max_id'];
        }

        return null;
    }

    /**
     * Récupère l'ID de la dernière réponse enregistrée.
     *
     * @return int|null L'ID de la dernière réponse ou null s'il n'y a pas de réponse enregistrée.
     */
    public function getDerniereReponse(): ?int
    {
        $query = "SELECT MAX(answer_id) AS max_id FROM ANSWER";
        $stmt = $this->conn->query($query);

        if ($stmt !== false) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            return $row['max_id'];
        }

        return null;
    }

    /**
     * Insère une nouvelle question dans la base de données.
     *
     * @param Question $question L'objet Question à insérer.
     * @throws \Exception Si la question avec l'ID donné n'existe pas.
     */
    public function insertQuestion(Question $question): void
    {
        $__QUIZZ__ = new QuizzDB($this->conn);
        $quizzId = $__QUIZZ__->getDernierQuiz();
        // Insérer la question dans la table QUESTION
        $query = "INSERT INTO QUESTION (question_text, type, score) VALUES (?, ?, ?)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $question->getText(), PDO::PARAM_STR);
        $stmt->bindParam(2, $question->getType(), PDO::PARAM_STR);
        $stmt->bindParam(3, $question->getScore(), PDO::PARAM_INT);
        $stmt->execute();
        $stmt = null;

        // Récupérer l'ID de la question insérée
        $questionId = $this->conn->lastInsertId();

        // Insérer les réponses associées dans la table ANSWER
        foreach ($question->getAnswers() as $answer) {
            $query = "INSERT INTO ANSWER (answer_text, question_id, est_valide) VALUES (?, ?, ?)";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(1, $answer->getAnswerText(), PDO::PARAM_STR);
            $stmt->bindParam(2, $questionId, PDO::PARAM_INT);
            $stmt->bindParam(3, $answer->isValid(), PDO::PARAM_BOOL);
            $stmt->execute();
            $stmt = null;
        }

        // Insérer l'association dans la table QUESTION_QUIZZ
        $query = "INSERT INTO QUESTION_QUIZZ (quizz_id, question_id) VALUES (?, ?)";
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $quizzId, PDO::PARAM_INT);
        $stmt->bindParam(2, $questionId, PDO::PARAM_INT);
        $stmt->execute();
        $stmt = null;
    }
}
?>
