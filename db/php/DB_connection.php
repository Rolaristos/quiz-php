<?php

namespace db\php;

use PDO;
use PDOException;

// Pour fonctionnement dans les ordinateurs à l'IUT
$servername = "servinfo-maria";
$username = "rakotomalala";
$password = "rakotomalala";
$database = "DBrakotomalala";

try {
    $cnx = new PDO("mysql:host=$servername;dbname=$database", $username, $password);

    $cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($cnx) {
        // echo "Connexion réussie";
    }
} catch (PDOException $e) {
    die("Connexion échouée: " . $e->getMessage());
}

?>

<?php
//namespace db\php;
//
//use mysqli;
//
//$servername = "localhost";
//$username = "rakotomalala";
//$password = "rakotomalala";
//$database = "MINAMI_NO_BD";
//
//Créer une connexion
//$cnx = new mysqli($servername, $username, $password, $database);
//
//Vérifier la connexion
//if ($cnx->connect_error) {
//    die("La connexion a échoué : " . $cnx->connect_error);
//}
//
//?>