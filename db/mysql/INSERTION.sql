-- INSERTIONS TABLE QUESTION AVEC ANSWER

-- TYPE QUESTION => RADIO

-- Question 1
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (1, 'Quel âge a Minami ?', 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (1, '16 ans', 1, TRUE),
        (2, '19 ans', 1, FALSE),
        (3, '14 ans', 1, FALSE),
        (4, '20 ans', 1, FALSE);

-- TYPE QUESTION => CHECKBOX

-- Question 2
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (2, 'Qui sont les proches de Minami ?', 'checkbox', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (5, 'Azumi', 2, TRUE),
        (6, 'Ayemi', 2, FALSE),
        (7, 'Narumi', 2, TRUE),
        (8, 'Shinsuke', 2, TRUE);

-- TYPE QUESTION => TEXT

-- Question 3
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (3, 'La couleur préférée de Minami ?', 'text', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (9, 'violet', 3, TRUE);


-- INSERTIONS TABLE QUIZZ AVEC QUESTION_QUIZZ

-- Quizz 1

INSERT INTO QUIZZ (quizz_id, quizz_name, quizz_description, quizz_difficulte)
VALUES  (1, "Entraînement!", "Ceci est un quizz d'introduction", 1);

INSERT INTO QUESTION_QUIZZ (quizz_id, question_id)
VALUES  (1, 1),
        (1, 2),
        (1, 3);

INSERT INTO USER (pseudo, mdp, est_admin)
VALUES  ("adm", "adm", TRUE);

-- TYPE QUESTION => RADIO

-- Question 4
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (4, 'Quelle est la nourriture préférée de Minami ?', 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (10, 'Sushi', 4, TRUE),
        (11, 'Pizza', 4, FALSE),
        (12, 'Ramen', 4, FALSE),
        (13, 'Burger', 4, FALSE);

-- TYPE QUESTION => CHECKBOX

-- Question 5
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (5, 'Quels sont les hobbies de Minami ?', 'checkbox', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (14, 'Lire', 5, TRUE),
        (15, 'Dessiner', 5, FALSE),
        (16, 'Jouer aux jeux vidéo', 5, FALSE),
        (17, 'Faire du sport', 5, TRUE);

-- TYPE QUESTION => TEXT

-- Question 6
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (6, 'Quel est le nom du meilleur ami(e) de Minami ?', 'text', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (18, 'Sanae', 6, TRUE);


-- INSERTIONS TABLE QUIZZ AVEC QUESTION_QUIZZ

-- Quizz 2

INSERT INTO QUIZZ (quizz_id, quizz_name, quizz_description, quizz_difficulte)
VALUES  (2, "Test de connaissances", "Quizz pour tester vos connaissances sur Minami", 2);

INSERT INTO QUESTION_QUIZZ (quizz_id, question_id)
VALUES  (2, 4),
        (2, 5),
        (2, 6);

-- TYPE QUESTION => RADIO

-- Question 7
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (7, 'Quel est le prénom du proviseur ?', 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (19, 'Miyako', 7, FALSE),
        (20, "La réponse n'est pas ici!", 7, TRUE),
        (21, 'Naoki', 7, FALSE),
        (22, 'Jun', 7, FALSE);

-- TYPE QUESTION => CHECKBOX

-- Question 8
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (8, 'Quels sont les hobbies de Tishiku ?', 'checkbox', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (23, 'Football', 8, TRUE),
        (24, 'Sortir dehors', 8, TRUE),
        (25, 'Cuisine', 8, FALSE),
        (26, 'Peinture', 8, FALSE);

-- TYPE QUESTION => TEXT

-- Question 9
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (9, "Quel est le sentiment de Minami lorsqu'elle découvre qu'elle est dans la même classe que Sanae ?", 'text', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (27, 'Gêne', 9, TRUE);


-- INSERTIONS TABLE QUIZZ AVEC QUESTION_QUIZZ

-- Quizz 3

INSERT INTO QUIZZ (quizz_id, quizz_name, quizz_description, quizz_difficulte, chapter_number)
VALUES  (3, 'Rentrée Scolaire', 'Quizz sur le chapitre 1 - La rentrée scolaire', 1, 1);

INSERT INTO QUESTION_QUIZZ (quizz_id, question_id)
VALUES  (3, 7),
        (3, 8),
        (3, 9);

-- INSERTIONS TABLE QUESTION AVEC ANSWER

-- Quizz 4 "Narumi vs Shinsuke"
INSERT INTO QUIZZ (quizz_id, quizz_name, quizz_description, quizz_difficulte, chapter_number)
VALUES  (4, "Narumi vs Shinsuke", "Chapitre 18 : Narumi vs Shinsuke", 2, 18);

-- Question 1
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (10, 'Que faisait Shinsuke à 4 heures du matin ?', 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (28, 'Il dormait', 10, TRUE),
       (29, 'Il était dehors', 10, FALSE),
       (30, 'Il travaillait', 10, FALSE);

-- Question 2
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (11, 'Où était Shinsuke à 9 heures du matin ?', 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (31, 'Au travail', 11, TRUE),
       (32, 'Il dormait encore', 11, FALSE),
       (33, 'Il était en train de cuisiner', 11, FALSE);

-- Question 3
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (12, 'A-t-il déjà embrassé Azumi ?', 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (34, 'Oui', 12, TRUE),
       (35, 'Non', 12, FALSE);

-- INSERTIONS TABLE QUIZZ AVEC QUESTION_QUIZZ

-- Quizz "Narumi vs Shinsuke"
INSERT INTO QUESTION_QUIZZ (quizz_id, question_id)
VALUES  (4, 10),
        (4, 11),
        (4, 12);

-- INSERTIONS TABLE QUESTION AVEC ANSWER

-- Quizz "Chien et chat"
INSERT INTO QUIZZ (quizz_id, quizz_name, quizz_description, quizz_difficulte, chapter_number)
VALUES  (5, "Chien et chat", "Chapitre 12 : Chien et chat", 3, 12);

-- Question 1
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (13, "Dans quel club Minami et Ayemi se rendent-elles après le club d'athlétisme ?", 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (36, 'Club de théâtre', 13, TRUE),
       (37, 'Club de cuisine', 13, FALSE),
       (38, 'Club de musique', 13, FALSE);

-- Question 2
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (14, 'Que propose Ayemi à Minami pour soulager son épaule blessée ?', 'checkbox', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (39, 'Un gros aimant', 14, TRUE),
       (40, 'Une compresse chaude', 14, FALSE),
       (41, 'Une crème apaisante', 14, FALSE);

-- INSERTIONS TABLE QUIZZ AVEC QUESTION_QUIZZ

-- Quizz "Chien et chat"
INSERT INTO QUESTION_QUIZZ (quizz_id, question_id)
VALUES  (5, 13),
        (5, 14);

-- Quizz "La classe 2-2"
INSERT INTO QUIZZ (quizz_id, quizz_name, quizz_description, quizz_difficulte, chapter_number)
VALUES  (6, "La classe 2-2", "Chapitre 2 : La classe 2-2", 2, 2);

-- Question 1
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (15, "Quel est le nom que Minami utilise pour se présenter ?", 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (42, 'Katayama Minamiii~', 15, TRUE),
       (43, 'Akane Ayemi', 15, FALSE),
       (44, 'Yano Tsuka', 15, FALSE);

-- Question 2
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (16, 'Quel est le groupe dont Tadao Nao est le chef adjoint ?', 'checkbox', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (45, 'Chika-Bankaa', 16, TRUE),
       (46, 'Super gang', 16, FALSE),
       (47, 'Chika-bankai', 16, FALSE);

-- Question 3
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (17, 'Quel est le club que Takunomi Asayoshi souhaite promouvoir ?', 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (48, 'Club de sport', 17, TRUE),
       (49, 'Club de journalisme', 17, FALSE),
       (50, 'Chika-Bankaa', 17, FALSE);

-- INSERTIONS TABLE QUIZZ AVEC QUESTION_QUIZZ

-- Quizz "La classe 2-2"
INSERT INTO QUESTION_QUIZZ (quizz_id, question_id)
VALUES  (6, 15),
        (6, 16),
        (6, 17);

-----------------------------------------------

-- INSERTIONS TABLE QUESTION AVEC ANSWER

-- Question 18
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (18, "Combien y a-t-il d'élèves dans la feuille donnée par le client ?", 'text', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (51, '27', 18, TRUE);

-- Question 19
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (19, "Où l'homme au costume dit travailler à la famille ?", 'checkbox', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (52, 'Orphelinat', 19, TRUE),
        (53, 'Kurokafu', 19, FALSE),
        (54, 'Egao no Ie', 19, TRUE),
        (55, 'Responsable', 19, FALSE);

-- Question 20
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (20, "Quel est le surnom donné au jeune garçon dans l'un des repères de Kurokafu ?", 'text', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES (56, 'Keima', 20, TRUE);

-- Question 21
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (21, "À quelle hiérarchie appartient l'assistante qui accompagne l'homme au costume et le jeune garçon ?", 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (57, 'Veuve noire', 21, FALSE),
        (58, 'Veuve', 21, FALSE),
        (59, 'Agent', 21, FALSE),
        (60, 'Veuvre Brune', 21, TRUE),
        (61, 'Caméléon', 21, FALSE);

-- Question 22
INSERT INTO QUESTION (question_id, question_text, type, score)
VALUES (22, "De quelle maladie est atteinte la femme du docteur ?", 'radio', 1);

INSERT INTO ANSWER (answer_id, answer_text, question_id, est_valide)
VALUES  (62, 'Cancer', 22, FALSE),
        (63, 'COVID-19', 22, FALSE),
        (64, 'Fièvre douloureuse', 22, FALSE),
        (65, 'Grippe à haut degré', 22, FALSE),
        (66, 'Stérile', 22, TRUE);


-- INSERTIONS TABLE QUIZZ AVEC QUESTION_QUIZZ

-- Quizz 7

INSERT INTO QUIZZ (quizz_id, quizz_name, quizz_description, quizz_difficulte, chapter_number)
VALUES  (7, "Prologue", "Plein de mystères tournent autour du chapitre 0. Mais... Aviez-vous au moins retenu les détails les plus simples ?", 4, 0);

INSERT INTO QUESTION_QUIZZ (quizz_id, question_id)
VALUES  (7, 18),
        (7, 19),
        (7, 20),
        (7, 21),
        (7, 22);
