const hideUlForOneSecond = () => {
    const ul = document.querySelector(".main-header ul");
    ul.style.display = "none";
    setTimeout(() => {
        ul.style.display = "flex";
    }, 1000);
};

window.addEventListener("load", () => {
    checkScreenSize();
});

window.addEventListener("resize", () => {
    checkScreenSize();
});

const checkScreenSize = () => {
    const screenSize = window.innerWidth;
    if (screenSize <= 1245) {
        hideUlForOneSecond();
    }
};