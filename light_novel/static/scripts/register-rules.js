$(document).ready(function () {
    $('#pseudo, #password, #confirmPassword').on('input', function () {
        validatePasswordMatch();
    });
    $('#confirmPassword').on('blur', function () {
        validatePasswordMatch();
    });
    $('#pseudo').on('input', function () {
        var username = $(this).val();

        if (username.length >= 6) {
            $.ajax({
                type: 'POST',
                url: '/light_novel/Query/check_username.php',
                data: { username: username },
                success: function (response) {
                    $('#availabilityMessage').html(response);
                }
            });
        } else {
            $('#availabilityMessage').html('');
        }
    });

    $('form').on('submit', function (event) {
        if (!validatePasswordMatch()) {
            event.preventDefault();
        }
    });

    function validatePasswordMatch() {
        var password = $('#password').val();
        var confirmPassword = $('#confirmPassword').val();

        if (password.trim() !== '' && confirmPassword.trim() !== '') {
            if (password !== confirmPassword) {
                $('#availabilityMessage').html('<span class="error-message" style="color:red;">Les mots de passe ne correspondent pas.</span>');
                return false;
            } else {
                $('#availabilityMessage').html('');
                return true;
            }
        } else {
            $('#availabilityMessage').html('');
            return true;
        }
    }
});
