const links = document.querySelectorAll('nav li');
var icons = document.getElementById('icons');
var nav = document.querySelector(".main-header");

icons.addEventListener("click", () => {
    nav.classList.toggle("isactive");
    icons.classList.toggle("rotate-animation-open");
    icons.classList.toggle("rotate-animation-close");
})

links.forEach((link) => {
    link.addEventListener("click", () => {
        nav.classList.remove("isactive");
        icons.classList.toggle("rotate-animation-open");
        icons.classList.toggle("rotate-animation-close");
    })
})