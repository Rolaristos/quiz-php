<?php
namespace light_novel\Query;
require_once __DIR__ . '/../../Quizz/Classes/Autoloader.php';
use \Quizz\Classes\Autoloader;

Autoloader::register();
require_once __DIR__ . '/../../db/php/DB_connection.php';
require_once __DIR__ . '/../../db/php/DBClasses/UserDB.php';

use db\php\DBClasses\UserDB;

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['username'])) {
    $username = $_POST['username'];
    $userDB = new UserDB($cnx);

    if ($userDB->userExists($username)) {
        echo '<span style="color: red;">Le pseudo est déjà pris.</span>';
    } else {
        echo '<span style="color: green;">Le pseudo est disponible.</span>';
    }
}

?>