<?php

namespace light_novel\Query;
session_start();

require_once __DIR__ . '/../../Quizz/Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

require_once __DIR__ . '/../../db/php/DB_connection.php';
require_once __DIR__ . '/../../Quizz/Classes/User/User.php';
require_once __DIR__ . '/../../db/php/DBClasses/UserDB.php';

use db\php\DBClasses\UserDB;
use Quizz\Classes\User\User;

$__USER__ = new UserDB($cnx);

$error = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["user"]) && isset($_POST["password"])) {
        $user = $_POST["user"];
        $motDePasse = $_POST["password"];
        if ($__USER__->userExists($user)) {
            $userObjet = $__USER__->getUserByPseudo($user);
            if ($user=="adm" && $userObjet->getMdp()==$motDePasse) {
                $loggedInUser = $userObjet;
                $serializedUser = serialize($loggedInUser);

                $_SESSION['user'] = $serializedUser;

                header("Location: /admin.php");
                exit;
            }
            if ($__USER__->checkPassword($user, $motDePasse)) {
                $loggedInUser = $__USER__->getUserByPseudo($user);
                $serializedUser = serialize($loggedInUser);

                $_SESSION['user'] = $serializedUser;

                header("Location: /light_novel/templates/minami_no_web.php");
                exit;
            } else {
                $error = "Mot de passe incorrect";
                header("Location: /light_novel/templates/login.php?error=" . urlencode($error));
                exit;
            }
        } else {
            $error = "Utilisateur non trouvé";
            header("Location: /light_novel/templates/login.php?error=" . urlencode($error));
            exit;
        }
    }
}
?>
