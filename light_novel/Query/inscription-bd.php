<?php
namespace light_novel\Query;

require_once __DIR__ . '/../../Quizz/Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

require_once __DIR__ . '/../../db/php/DB_connection.php';
require_once __DIR__ . '/../../Quizz/Classes/User/User.php';
require_once __DIR__ . '/../../db/php/DBClasses/UserDB.php';


use db\php\DBClasses\UserDB;
use Quizz\Classes\User\User;

$__USER__ = new UserDB($cnx);

?>
<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["pseudo"]) && isset($_POST["password"])) {
        $pseudo = $_POST["pseudo"];
        $motDePasse = $_POST["password"];
        if ($__USER__->userExists($pseudo)) {
            header("Location: /light_novel/templates/register.php");
            exit();
        } else {
            $__USER__->addUser(new User($pseudo, $motDePasse));
            header("Location: /light_novel/templates/login.php");
            exit(); 
        }
    }
}
?>