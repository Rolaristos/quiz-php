<?php
    namespace light_novel\Query;
    session_start();
    session_destroy();
    header("Location: /light_novel/templates/login.php");
    exit;
?>
