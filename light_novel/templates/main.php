<?php
    namespace light_novel\templates;
?>

<main class="main-main">
    <?php $randomValue = mt_rand(-9, 8); ?>
    <div class="image-minami object" data-value="<?php echo $randomValue;?>"></div>
</main>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        setTimeout(function () {
            document.querySelector('.image-minami').classList.add('visible');
        }, 1000);
    });
</script>