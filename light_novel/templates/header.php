<?php
namespace light_novel\templates;

$userIsLoggedIn = isset($_SESSION['user']);
$linktoaccueil = "/light_novel/templates/minami_no_web.php";
$linktoquizz = "/Quizz/templates/minami-quizz-menu.php";
$linktologin = "/light_novel/templates/login.php";
?>

<header>
    <nav class="main-header">
        <div class="just-hover">
            <div id="icons" class="rotate-animation-close"></div>
        </div>
        <ul>
            <li><a href="<?php echo $linktoaccueil ?>">Accueil</a></li>
            <li><a href="#">Chapitres</a></li>
            <li><a href="#">Personnages</a></li>
            <?php
            if ($userIsLoggedIn) { // Vérification si le user est connecté
                echo '<li><a href=' . $linktoquizz . '>Quizz</a></li>';
            } else {
                echo '<li><a href=' . $linktologin . '>Quizz</a></li>';
            }
            ?>
        </ul>
        <?php 
            if ($userIsLoggedIn) { // Vérification si le user est connecté
                echo '<a href="/light_novel/Query/logout.php"><button class="connect-link" onclick="tologin()">Se déconnecter</button></a>';
            } else {
                echo '<a href=' . $linktologin . '><button class="connect-link" onclick="tologin()">Se connecter</button></a>';
            }
        ?>
    </nav>
</header>

<!-- Script pour la page minami_no_web.php -->
<script src="../static/scripts/main-burger-menu.js"></script>
<script src="../static/scripts/hide-ul-for-one-second.js"></script>

<!-- Script pour la page minami_quizz.php -->
<script src="../../light_novel/static/scripts/main-burger-menu.js"></script>
<script src="../../light_novel/static/scripts/hide-ul-for-one-second.js"></script>