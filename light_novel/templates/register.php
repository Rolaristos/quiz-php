<?php
namespace light_novel\templates;

require_once __DIR__ . '/../../Quizz/Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

require __DIR__ . '/../../db/php/DB_connection.php';
require __DIR__ . '/../../db/php/DBClasses/QuestionDB.php';
require __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';
require __DIR__ . '/../../Quizz/Classes/User/User.php';
require __DIR__ . '/../../db/php/DBClasses/UserDB.php';


use db\php\DBClasses\UserDB;
use Quizz\Classes\User\User;

// Création d'une instance de la classe UserDB
$__USER__ = new UserDB($cnx);

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Minami no hanashi - Inscription</title>
    <link rel="icon" href="../static/images/icon/flavicon.svg" type="image/svg+xml">
    <link rel="stylesheet" href="../static/styles/body.css">
    <link rel="stylesheet" href="../static/styles/register.css">
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <style>
        #availabilityMessage {
            position: absolute;
            display: block;
            text-align: center;
            width: calc(100% - 40px);
            top: 19%;
        }
    </style>
    <script src="../static/scripts/register-rules.js"></script>
    <script>
    document.addEventListener('DOMContentLoaded', function() {
        // Récupération de tous les conteneurs d'entrée
        const inputContainers = document.querySelectorAll('.input-container');

        // Parcours de chaque conteneur d'entrée
        inputContainers.forEach(container => {
            const input = container.querySelector('.input');
            const label = container.querySelector('.iLabel');

            // Écouteur d'événement pour la saisie utilisateur
            input.addEventListener('input', function() {
                if (input.value.trim() !== '') {
                    container.classList.add('input-filled');
                } else {
                    container.classList.remove('input-filled');
                }
            });

            // Vérification de la valeur au chargement de la page
            if (input.value.trim() !== '') {
                container.classList.add('input-filled');
            }
        });
    });
    </script>
</head>
<body class="background-filter">
    <div class="content">
        <!-- Formulaire d'inscription -->
        <form action="/light_novel/Query/inscription-bd.php" method="POST">
            <div class="form">
            <a href="login.php" class="back-link">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                    <path d="M10.736 1.427a1.5 1.5 0 0 1 2.122 2.122L5.05 8l7.808 7.778a1.5 1.5 0 1 1-2.122 2.122l-8.25-8.25a1.5 1.5 0 0 1 0-2.122l8.25-8.25z"/>
                </svg>
                Retour
            </a>
                <div class="title">Bienvenue</div>
                <span id="availabilityMessage"></span>
                <div class="input-container ic1">
                    <input placeholder="" type="text" class="input" id="pseudo" name="pseudo" required pattern="[A-Za-z0-9]{6,}" title="Le pseudo doit avoir au moins 6 caractères et ne doit pas contenir de caractères spéciaux.">
                    <div class="cut"></div>
                    <label class="iLabel" for="pseudo">Pseudo</label>
                    <span id="availabilityMessage"></span>
                </div>

                <div class="input-container ic2">
                    <input placeholder="" type="password" class="input" id="password" name="password" required>
                    <div class="cut"></div>
                    <label class="iLabel" for="password">Mot de passe</label>
                </div>

                <div class="input-container ic2">
                    <input placeholder="" type="password" class="input" id="confirmPassword" name="confirmPassword" required>
                    <div class="cut cut-short"></div>
                    <label class="iLabel" for="confirmPassword">Confirmer mot de passe</label>
                </div>

                <button class="submit" type="submit">S'inscrire</button>
            </div>
        </form>
    </div>
</body>
</html>
