<?php
namespace light_novel\templates;
session_start();


require_once __DIR__ . '/../../Quizz/Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

require __DIR__ . '/../../db/php/DB_connection.php';
require __DIR__ . '/../../db/php/DBClasses/QuestionDB.php';
require __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';
require __DIR__ . '/../../Quizz/Classes/User/User.php';
require __DIR__ . '/../../db/php/DBClasses/UserDB.php';

use db\php\DBClasses\UserDB;
use Quizz\Classes\User\User;

// Création d'une instance de la classe UserDB
$__USER__ = new UserDB($cnx);

?>

<!DOCTYPE html>
<html lang="fr" style="height:100%;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Minami no hanashi</title>
    <link rel="icon" href="../static/images/icon/flavicon.svg" type="image/svg+xml">
    <link rel="stylesheet" href="../static/styles/body.css">
    <link rel="stylesheet" href="../static/styles/header.css">
    <link rel="stylesheet" href="../static/styles/main.css">
</head>

<body style="height:100%; margin:0;">
    <div class="background-filter">
        <?php
        require_once 'header.php';
        require_once 'main.php';

        // Vérification si un utilisateur est connecté
        if (isset($_SESSION['user'])) {
            // Récupération de l'utilisateur connecté depuis la session
            $serializedUser = $_SESSION['user'];
            $loggedInUser = unserialize($serializedUser);
            // Affichage des informations de l'utilisateur (pseudo et mot de passe)
            echo '<p>Pseudo: ' . htmlspecialchars($loggedInUser->getPseudo()) . '</p>';
            echo '<p>Mot de passe: ' . htmlspecialchars($loggedInUser->getMdp()) . '</p>';
        }
        ?>
    </div>
</body>
</html>
