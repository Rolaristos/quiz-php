<?php
namespace light_novel\templates;

require_once __DIR__ . '/../../Quizz/Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

require __DIR__ . '/../../db/php/DB_connection.php';
require __DIR__ . '/../../db/php/DBClasses/QuestionDB.php';
require __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';
require __DIR__ . '/../../Quizz/Classes/User/User.php';
require __DIR__ . '/../../db/php/DBClasses/UserDB.php';


use db\php\DBClasses\UserDB;
use Quizz\Classes\User\User;

$__USER__ = new UserDB($cnx);

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Minami no hanashi - Connexion</title>
    <link rel="icon" href="../static/images/icon/flavicon.svg" type="image/svg+xml">
    <link rel="stylesheet" href="../static/styles/login.css">
    <link rel="stylesheet" href="../static/styles/body.css">
</head>
<body class="background-filter">
    <div class="content">
        <form class="form" action="/light_novel/Query/login-bd.php" method="POST">
            <a href="minami_no_web.php" class="back-link">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                    <path d="M10.736 1.427a1.5 1.5 0 0 1 2.122 2.122L5.05 8l7.808 7.778a1.5 1.5 0 1 1-2.122 2.122l-8.25-8.25a1.5 1.5 0 0 1 0-2.122l8.25-8.25z"/>
                </svg>
                Retour
            </a>
            <p id="heading">Connexion</p>
            <?php if (isset($_GET['error']) && !empty($_GET['error'])) : ?>
                <p class="error-message"><?php echo htmlspecialchars($_GET['error']); ?></p>
            <?php endif; ?>
            <div class="field">
            <svg class="input-icon" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
            <path d="M13.106 7.222c0-2.967-2.249-5.032-5.482-5.032-3.35 0-5.646 2.318-5.646 5.702 0 3.493 2.235 5.708 5.762 5.708.862 0 1.689-.123 2.304-.335v-.862c-.43.199-1.354.328-2.29.328-2.926 0-4.813-1.88-4.813-4.798 0-2.844 1.921-4.881 4.594-4.881 2.735 0 4.608 1.688 4.608 4.156 0 1.682-.554 2.769-1.416 2.769-.492 0-.772-.28-.772-.76V5.206H8.923v.834h-.11c-.266-.595-.881-.964-1.6-.964-1.4 0-2.378 1.162-2.378 2.823 0 1.737.957 2.906 2.379 2.906.8 0 1.415-.39 1.709-1.087h.11c.081.67.703 1.148 1.503 1.148 1.572 0 2.57-1.415 2.57-3.643zm-7.177.704c0-1.197.54-1.907 1.456-1.907.93 0 1.524.738 1.524 1.907S8.308 9.84 7.371 9.84c-.895 0-1.442-.725-1.442-1.914z"></path>
            </svg>
            <input autocomplete="off" placeholder="Username" class="input-field" type="text" name="user" required>
            </div>
            <div class="field">
            <svg class="input-icon" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" viewBox="0 0 16 16">
            <path d="M8 1a2 2 0 0 1 2 2v4H6V3a2 2 0 0 1 2-2zm3 6V3a3 3 0 0 0-6 0v4a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2z"></path>
            </svg>
            <input placeholder="Mot de passe" class="input-field" type="password" name="password" required>
            </div>
            <div class="btn">
            <button class="button1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Se connecter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
            </div>
            <p class="sinscrire">S'inscrire <a href="register.php">ici</a></p>
        </form>
    </div>
</body>
</html>