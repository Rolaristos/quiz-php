let answerCounts = {};
let questionCount = 0;

function addAnswerField(questionIndex) {
    if (!answerCounts[questionIndex]) {
        answerCounts[questionIndex] = 1;
    }

    const container = document.getElementById(`answerContainer_${questionIndex}`);

    const newLabel = document.createElement('label');
    newLabel.textContent = `Réponse ${answerCounts[questionIndex]} :`;
    const newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.id = `answerTextArray_${questionIndex}_${answerCounts[questionIndex]}[]`;
    newInput.name = `answerTextArray_${questionIndex}_${answerCounts[questionIndex]}[]`;
    newInput.required = true;

    const newValidityLabel = document.createElement('label');
    newValidityLabel.textContent = 'Valide :';
    const newValidityInput = document.createElement('input');
    newValidityInput.type = 'checkbox';
    newValidityInput.id = `isValidArray_${questionIndex}_${answerCounts[questionIndex]}[]`;
    newValidityInput.name = `isValidArray_${questionIndex}_${answerCounts[questionIndex]}[]`;
    newValidityInput.value = answerCounts[questionIndex];

    container.appendChild(newLabel);
    container.appendChild(newInput);
    container.appendChild(newValidityLabel);
    container.appendChild(newValidityInput);
    container.appendChild(document.createElement('br'));

    answerCounts[questionIndex]++;
}

function removeAnswerField(questionIndex) {
    if (answerCounts[questionIndex] > 2) {
        const container = document.getElementById(`answerContainer_${questionIndex}`);
        for (let i = 0; i < 5; i++) {
            container.removeChild(container.lastChild);
        }
        answerCounts[questionIndex]--;
    }
}

function addQuestionField() {
    questionCount++;
    const container = document.getElementById('questionContainer');

    const questionIndex = questionCount;

    const newQuestionLabel = document.createElement('label');
    container.appendChild(document.createElement('br'));
    newQuestionLabel.textContent = `Question ${questionIndex} :`;
    const newQuestionInput = document.createElement('input');
    newQuestionInput.type = 'text';
    newQuestionInput.id = `questionTextArray_${questionIndex}[]`;
    newQuestionInput.name = `questionTextArray_${questionIndex}[]`;
    newQuestionInput.required = true;

    const newTypeLabel = document.createElement('label');
    newTypeLabel.textContent = 'Type de la question :';
    const newTypeSelect = document.createElement('select');
    newTypeSelect.id = `questionTypeArray_${questionIndex}[]`;
    newTypeSelect.name = `questionTypeArray_${questionIndex}[]`;
    newTypeSelect.required = true;

    const radioOption = document.createElement('option');
    radioOption.value = 'radio';
    radioOption.textContent = 'Choix unique (Radio)';
    newTypeSelect.appendChild(radioOption);

    const checkboxOption = document.createElement('option');
    checkboxOption.value = 'checkbox';
    checkboxOption.textContent = 'Choix multiple (Checkbox)';
    newTypeSelect.appendChild(checkboxOption);

    const textOption = document.createElement('option');
    textOption.value = 'text';
    textOption.textContent = 'Réponse textuelle';
    newTypeSelect.appendChild(textOption);

    const newScoreLabel = document.createElement('label');
    newScoreLabel.textContent = 'Score de la question :';
    const newScoreInput = document.createElement('input');
    newScoreInput.type = 'number';
    newScoreInput.id = `questionScoreArray_${questionIndex}[]`;
    newScoreInput.name = `questionScoreArray_${questionIndex}[]`;
    newScoreInput.required = true;
    newScoreInput.min = 0;
    newScoreInput.max = 10000;

    container.appendChild(newQuestionLabel);
    container.appendChild(newQuestionInput);

    container.appendChild(newTypeLabel);
    container.appendChild(newTypeSelect);

    container.appendChild(newScoreLabel);
    container.appendChild(newScoreInput);
    container.appendChild(document.createElement('br'));

    // Initialiser le compteur de réponses pour la nouvelle question
    answerCounts[questionIndex] = 1;

    // Conteneur pour les réponses de la nouvelle question
    const answerContainer = document.createElement('div');
    answerContainer.id = `answerContainer_${questionIndex}`;
    container.appendChild(answerContainer);

    // Ajouter également les éléments pour la réponse initiale de la nouvelle question
    addAnswerField(questionIndex);

    // Bouton pour ajouter une réponse à la nouvelle question
    const addAnswerButton = document.createElement('button');
    addAnswerButton.type = 'button';
    addAnswerButton.textContent = 'Ajouter une réponse';
    addAnswerButton.onclick = function () {
        addAnswerField(questionIndex);
    };
    container.appendChild(addAnswerButton);

    // Bouton pour supprimer la dernière réponse de la nouvelle question
    const removeAnswerButton = document.createElement('button');
    removeAnswerButton.type = 'button';
    removeAnswerButton.textContent = 'Supprimer la dernière réponse';
    removeAnswerButton.onclick = function () {
        removeAnswerField(questionIndex);
    };
    container.appendChild(removeAnswerButton);

    // Ajouter un champ de formulaire pour le nombre de questions
    const questionCountInput = document.createElement('input');
    questionCountInput.type = 'hidden';
    questionCountInput.name = 'questionCount';
    questionCountInput.value = questionCount;
    container.appendChild(questionCountInput);
}

function removeQuestionField() {
    if (questionCount > 0) {
        const container = document.getElementById('questionContainer');

        // Identifier l'index de la dernière question
        const lastQuestionIndex = questionCount;

        // Supprimer tous les éléments liés à la dernière question
        for (let i = 0; i < 11; i++) {
            container.removeChild(container.lastChild);
        }

        if (container.lastChild && container.lastChild.tagName === "BR") {
            container.removeChild(container.lastChild);
        }

        // Supprimer également le suivi du nombre de réponses pour la dernière question
        delete answerCounts[lastQuestionIndex];

        questionCount--;
    }
}

function collectFormData() {
    const formData = {};

    for (let i = 1; i <= questionCount; i++) {
        const questionTextElement = document.getElementById(`questionTextArray_${i}[]`);
        console.log(questionTextElement);

        const questionText = questionTextElement ? questionTextElement.value : '';
        const questionType = document.getElementById(`questionTypeArray_${i}[]`).value;
        const questionScore = document.getElementById(`questionScoreArray_${i}[]`).value;

        const answers = [];
        for (let j = 1; j <= answerCounts[i]; j++) {
            const answerTextElement = document.getElementById(`answerTextArray_${i}_${j}[]`);
            console.log(answerTextElement);

            const answerText = answerTextElement ? answerTextElement.value : '';
            const isValidElement = document.getElementById(`isValidArray_${i}_${j}[]`);
            const isValid = isValidElement ? isValidElement.checked : false;

            answers.push({
                answerText,
                isValid
            });
        }

        formData[`question_${i}`] = {
            questionText,
            questionType,
            questionScore,
            answers
        };
    }

    return formData;
}


function submitForm() {
    // Collecter les données du formulaire
    const formData = collectFormData();

    // Vérifier si tous les champs de texte de question et de réponse sont remplis
    const isFormValid = Object.values(formData).every(question => {
        // Vérifier le champ de texte de question
        const isQuestionValid = question.questionText.trim() !== '';

        // Vérifier les champs de texte de réponse
        const areAnswersValid = question.answers.every(answer => answer.answerText.trim() !== '');

        return isQuestionValid && areAnswersValid;
    });

    if (!isFormValid) {
        alert('Les questions seront associés au Quiz crée précedemment. Les réponses n ayant aucune bonne réponses associés seront malheureusement prise en compte (Cette fonctionnalité est encore en BETA)');
        return; // Ne pas soumettre le formulaire si des champs sont vides
    }

    // Ajouter le champ de formulaire pour le nombre de questions
    const questionCountInput = document.createElement('input');
    questionCountInput.type = 'hidden';
    questionCountInput.name = 'questionCount';
    questionCountInput.value = questionCount;
    document.getElementById('questionForm').appendChild(questionCountInput);

    console.log(formData);

    // Soumettre le formulaire
    const form = document.getElementById('questionForm');
    form.submit();
}

document.getElementById('createQuestionButton').addEventListener('click', submitForm);
