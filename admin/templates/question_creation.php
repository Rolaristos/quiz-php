<?php
namespace admin\templates;

require_once __DIR__ . '/../../db/php/DB_connection.php';
require_once __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';
require_once __DIR__ . '/../../db/php/DBClasses/QuestionDB.php';

use db\php\DBClasses\QuizzDB;
use db\php\DBClasses\QuestionDB;

$__QUESTION__ = new QuestionDB($cnx);
?>

<form method="GET" id="questionForm" action="/admin/request/question_creation_bd.php">
    <!-- Bouton pour ajouter une question -->
    <button type="button" onclick="addQuestionField()">Ajouter une question</button>

    <!-- Bouton pour supprimer la dernière question -->
    <button type="button" onclick="removeQuestionField()">Supprimer la dernière question</button>

    <!-- Conteneur pour les questions -->
    <div id="questionContainer"></div>

    <input type="submit" id="createQuestionButton" name="createQuestion" value="Créer la/les question(s)">

    <!-- Script JavaScript pour l'ajout dynamique et suppression de champs de réponse et de question -->
    <script src="../static/script/ajout-suppression-q&a.js"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Démarrer la vérification en boucle
            setInterval(checkQuestionContainer, 1000); // Vérifier toutes les 1000 millisecondes (1 seconde)
        });

        function checkQuestionContainer() {
            const createQuestionButton = document.getElementById('createQuestionButton');
            const questionContainer = document.getElementById('questionContainer');

            // Désactiver le bouton si le conteneur de questions est vide
            createQuestionButton.disabled = isContainerEmpty(questionContainer);
        }

        function isContainerEmpty(container) {
            return container.childElementCount === 0;
        }
    </script>
</form>
