<?php
namespace admin\templates;
require_once __DIR__ . '/../../db/php/DB_connection.php';
require_once __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';
require_once __DIR__ . '/../../db/php/DBClasses/QuestionDB.php';

use db\php\DBClasses\QuizzDB;
use db\php\DBClasses\QuestionDB;

$__QUESTION__ = new QuestionDB($cnx);

// Initialisez la classe QuizzDB
$quizzDB = new QuizzDB($cnx);

// Vérifiez si l'action et l'ID sont définis dans l'URL
if (isset($_GET['action']) && isset($_GET['id'])) {
    $action = $_GET['action'];
    $quizId = (int)$_GET['id'];

    switch ($action) {
        case 'viewQuestions':
            // Récupérez le quizz en fonction de l'ID
            $quizz = $quizzDB->getQuizzById($quizId);

            // Vérifiez si le quizz existe
            if ($quizz) {
                // Récupérez les questions associées au quizz
                $questions = $quizzDB->addQuestions($quizId);

                // Affichez les informations du quizz et les questions associées
                echo "<style>
                    body {
                        background-color: #222;
                        color: #fff;
                        font-family: 'Arial', sans-serif;
                    }

                    h2 {
                        color: #64a19d;
                    }

                    p, a{
                        color: #ccc;
                    }

                    ul {
                        list-style-type: none;
                        padding: 0;
                    }

                    li {
                        margin-bottom: 10px;
                    }
                </style>";
                echo '<a href="/admin.php">Page admin</a>';
                echo "<h2>Questions pour le quizz : {$quizz->getName()}</h2>";
                echo "<p>Description : {$quizz->getDescription()}</p>";
                echo "<p>Difficulté : {$quizz->getDifficulte()}</p>";

                // Affichez la liste des questions
                echo "<ul>";
                foreach ($questions as $question) {
                    echo "<li>{$question->getText()}</li>";
                }
                echo "</ul>";
            } else {
                echo "<p>Le quizz demandé n'existe pas.</p>";
            }
            break;

        default:
            echo "<p>Action non reconnue.</p>";
            break;
    }
} else {
    echo "<p>Paramètres manquants dans l'URL.</p>";
}
?>
