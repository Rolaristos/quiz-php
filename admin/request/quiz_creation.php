<?php
namespace admin\request;

require_once __DIR__ . '/../../db/php/DB_connection.php';
require_once __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';

use db\php\DBClasses\QuizzDB;

if (isset($_POST['createQuiz'])) {
    $quizName = $_POST['quizName'];
    $quizDescription = $_POST['quizDescription'];
    $quizDifficulty = $_POST['quizDifficulty'];

    $quizzDB = new QuizzDB($cnx);

    $quizzDB->createQuiz($quizName, $quizDescription, $quizDifficulty);

    header("Location: /admin/templates/question_creation.php");
    exit();
}
?>
