<?php
namespace admin\request;

require_once __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';

use db\php\DBClasses\QuizzDB;

$action = isset($_GET['action']) ? $_GET['action'] : null;
$quizId = isset($_GET['id']) ? $_GET['id'] : null;

if ($action === 'deleteQuiz' && $quizId !== null) {

    $quizzDB = new QuizzDB($cnx);
    $quizzDB->deleteQuizById($quizId);

    header("Location: /admin.php");
    exit;
}


?>