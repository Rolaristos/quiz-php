<?php
namespace admin\request;

require_once __DIR__ . '/../../db/php/DB_connection.php';
require_once __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';
require_once __DIR__ . '/../../db/php/DBClasses/QuestionDB.php';
require_once __DIR__ . '/../../Quizz/Classes/Question/QuestionRadio.php';
require_once __DIR__ . '/../../Quizz/Classes/Question/QuestionCheckbox.php';
require_once __DIR__ . '/../../Quizz/Classes/Question/QuestionText.php';
require_once __DIR__ . '/../../Quizz/Classes/Answer/Answer.php';

use db\php\DBClasses\QuizzDB;
use db\php\DBClasses\QuestionDB;
use Quizz\Classes\Question\QuestionText;
use Quizz\Classes\Question\QuestionRadio;
use Quizz\Classes\Question\QuestionCheckbox;
use Quizz\Classes\Answer\Answer;

// Vérification de la soumission du formulaire de création de quiz
if (isset($_POST['createQuiz'])) {
    // Récupération des données du formulaire
    $quizName = $_POST['quizName'];
    $quizDescription = $_POST['quizDescription'];
    $quizDifficulty = $_POST['quizDifficulty'];

    // Création des instances de base de données
    $quizzDB = new QuizzDB($cnx);
    $questionDB = new QuestionDB($cnx);

    $quizzDB->createQuiz($quizName, $quizDescription, $quizDifficulty);

    // Redirection vers la page de création de questions
    header("Location: /admin/templates/question_creation.php");
    exit();
}

// Vérification de la soumission du formulaire de création de questions
if (isset($_GET['createQuestion'])) {
    // Récupération du nombre de questions
    $questionCount = $_GET['questionCount'];
    $questionDB = new QuestionDB($cnx);

    for ($i = 1; $i <= $questionCount; $i++) {
        $questionText = $_GET["questionTextArray_{$i}"][0];
        $questionType = $_GET["questionTypeArray_{$i}"][0];
        $questionScore = $_GET["questionScoreArray_{$i}"][0];

        $name = $questionDB->getDerniereQuestionId() + $i;

        // Filtrage des clés des réponses pour la question actuelle
        $answers = [];
        $choices = [];
        $answerKeys = array_filter(array_keys($_GET), function ($key) use ($i) {
            return strpos($key, "answerTextArray_{$i}_") === 0;
        });

        echo "<p>questionText: $questionText</p>";
        echo "<p>questionType: $questionType</p>";
        echo "<p>questionScore: $questionScore</p>";

        $answerCount = count($answerKeys);

        for ($j = 1; $j <= $answerCount; $j++) {
            // Récupération des informations de la réponse
            $answerText = $_GET["answerTextArray_{$i}_{$j}"][0];
            $isValid = isset($_GET["isValidArray_{$i}_{$j}"][0]) ? $_GET["isValidArray_{$i}_{$j}"][0] : false;
            
            // Conversion de $isValid en booléen
            if (is_string($isValid)) {
                $isValid = true;
            }
            // Conversion de $isValid en booléen (cas spécifique à 1)
            switch ($isValid) {
                case 1:
                    $isValid = TRUE;
                    break;

                default:
                    $isValid = FALSE;
                    break;
            }

            $answer = new Answer($name, $answerText, $name, $isValid);
            $answers[] = $answer;

            echo "<p>answerText: $answerText</p>";
            echo "<p>isValid: $isValid</p>";
        }

        // Création de l'instance de question en fonction du type
        switch ($questionType) {
            case 'radio':
                $question = new QuestionRadio((string) $name, $questionType, $questionText, $answers, (int) $questionScore, $choices);
                break;

            case 'checkbox':
                $question = new QuestionCheckbox((string) $name, $questionType, $questionText, $answers, (int) $questionScore, $choices);
                break;

            case 'text':
                $question = new QuestionText((string) $name, $questionType, $questionText, $answers, (int) $questionScore);
                break;

            default:
                throw new \Exception("Type de question non pris en charge : $questionType");
        }

        $questionDB->insertQuestion($question);
    }
    // Redirection vers la page d'administration
    header("Location: /admin.php");
    exit();

}
?>
