document.addEventListener("DOMContentLoaded", function () {
    var load = document.querySelector('.loading-animation');
    var main = document.querySelector('.main-content');
    var underBody = document.querySelector('.background-filter');

    if (underBody.classList.contains('loading')) {
        main.style.visibility = 'hidden';
        setTimeout(function () {
            load.classList.add('active');
            setTimeout(function () {
                load.classList.add('active-end');
                setTimeout(function () {
                    load.style.display = 'none';
                    main.style.visibility = 'visible';
                    main.style.opacity = '1';
                    underBody.style.overflow = "auto";
                }, 800);
            }, 5000);
        }, 500);
        simulateLoading();
    } else {
        console.log("Chargement déjà terminé.");
        load.style.display = 'none';
        main.style.visibility = 'visible';
        main.style.opacity = '1';
        underBody.style.overflow = "auto";
    }
});

function simulateLoading() {
    var loadingBar = document.getElementById("loading-bar");
    var width = 0;
    var interval = setInterval(function () {
        if (width >= 100) {
            clearInterval(interval);
            console.log("Chargement terminé!");
        } else {
            width++;
            loadingBar.style.width = width + "%";
        }
    }, 50);
}
