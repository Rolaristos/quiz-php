<?php
namespace Quizz\Classes\Answer;

/**
 * La classe Answer représente une option de réponse pour une question de quiz.
 */
class Answer
{
    /**
     * @var int L'identifiant unique de la réponse.
     */
    private int $answerId;

    /**
     * @var string Le contenu textuel de la réponse.
     */
    private string $answerText;

    /**
     * @var int L'identifiant de la question à laquelle cette réponse appartient.
     */
    private int $questionId;

    /**
     * @var bool Indique si la réponse est considérée comme valide.
     */
    private bool $isValid;

    /**
     * Constructeur de la classe Answer.
     *
     * @param int    $answerId    L'identifiant unique de la réponse.
     * @param string $answerText  Le contenu textuel de la réponse.
     * @param int    $questionId  L'identifiant de la question à laquelle cette réponse appartient.
     * @param bool   $isValid     Indique si la réponse est considérée comme valide (par défaut à false).
     */
    public function __construct(int $answerId, string $answerText, int $questionId, bool $isValid = false)
    {
        $this->answerId = $answerId;
        $this->answerText = $answerText;
        $this->questionId = $questionId;
        $this->isValid = $isValid;
    }

    /**
     * Obtient l'identifiant de la réponse.
     *
     * @return int L'identifiant de la réponse.
     */
    public function getAnswerId(): int
    {
        return $this->answerId;
    }

    /**
     * Définit l'identifiant de la réponse.
     *
     * @param int $answerId L'identifiant de la réponse.
     */
    public function setAnswerId(int $answerId): void
    {
        $this->answerId = $answerId;
    }

    /**
     * Obtient le contenu textuel de la réponse.
     *
     * @return string Le contenu textuel de la réponse.
     */
    public function getAnswerText(): string
    {
        return $this->answerText;
    }

    /**
     * Définit le contenu textuel de la réponse.
     *
     * @param string $answerText Le contenu textuel de la réponse.
     */
    public function setAnswerText(string $answerText): void
    {
        $this->answerText = $answerText;
    }

    /**
     * Obtient l'identifiant de la question à laquelle la réponse appartient.
     *
     * @return int L'identifiant de la question.
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * Définit l'identifiant de la question à laquelle la réponse appartient.
     *
     * @param int $questionId L'identifiant de la question.
     */
    public function setQuestionId(int $questionId): void
    {
        $this->questionId = $questionId;
    }

    /**
     * Vérifie si la réponse est considérée comme valide.
     *
     * @return bool True si la réponse est valide, sinon false.
     */
    public function isValid(): bool
    {
        return $this->isValid;
    }

    /**
     * Définit si la réponse est valide ou non.
     *
     * @param bool $isValid Indique si la réponse est valide.
     */
    public function setValid(bool $isValid): void
    {
        $this->isValid = $isValid;
    }

    /**
     * Renvoie le contenu textuel de la réponse (utilisé pour l'affichage).
     *
     * @return string Le contenu textuel de la réponse.
     */
    public function render(): string
    {
        return $this->answerText;
    }
}
?>
