<?php

namespace Quizz\Classes;
class Autoloader
{
    public static function register()
    {
        spl_autoload_register(function ($class) {
            // Remplace le préfixe du namespace avec le chemin relatif
            if (strpos($class, "DB") !== false) {
                $file = str_replace('Quizz\\Classes\\' ,'' ,__DIR__) . str_replace('\\', '/', $class) . ".php";
            } else {
                $class = str_replace('Quizz\Classes\\', '', $class);
                $file = __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';
            }
            

            // Inclut le fichier s'il existe
            if (file_exists($file)) {
                require_once $file;
            }
        });
    }
}

Autoloader::register();
?>