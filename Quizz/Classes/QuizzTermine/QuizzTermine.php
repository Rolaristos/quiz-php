<?php
namespace Quizz\Classes\QuizzTermine;

/**
 * La classe QuizzTermine représente les résultats d'un quiz terminé par un utilisateur.
 */
class QuizzTermine
{
    /**
     * @var string Le pseudo de l'utilisateur.
     */
    private string $pseudo;

    /**
     * @var int L'identifiant du quiz terminé.
     */
    private int $quizzId;

    /**
     * @var int Le score obtenu par l'utilisateur.
     */
    private int $score;

    /**
     * Constructeur de la classe QuizzTermine.
     *
     * @param string $pseudo   Le pseudo de l'utilisateur.
     * @param int    $quizzId  L'identifiant du quiz terminé.
     * @param int    $score    Le score obtenu par l'utilisateur.
     */
    public function __construct(string $pseudo, int $quizzId, int $score)
    {
        $this->pseudo = $pseudo;
        $this->quizzId = $quizzId;
        $this->score = $score;
    }

    /**
     * Obtient le pseudo de l'utilisateur.
     *
     * @return string Le pseudo de l'utilisateur.
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Obtient l'identifiant du quiz terminé.
     *
     * @return int L'identifiant du quiz terminé.
     */
    public function getQuizzId()
    {
        return $this->quizzId;
    }

    /**
     * Obtient le score obtenu par l'utilisateur.
     *
     * @return int Le score obtenu par l'utilisateur.
     */
    public function getScore()
    {
        return $this->score;
    }
}
?>
