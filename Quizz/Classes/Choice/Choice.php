<?php
namespace Quizz\Classes\Choice;

/**
 * La classe Choice représente une option de choix dans un quiz.
 */
class Choice
{
    /**
     * @var string Le texte de l'option de choix.
     */
    private string $text;

    /**
     * @var string La valeur associée à l'option de choix.
     */
    private string $value;

    /**
     * Constructeur de la classe Choice.
     *
     * @param string $text  Le texte de l'option de choix.
     * @param string $value La valeur associée à l'option de choix.
     */
    public function __construct(string $text, string $value)
    {
        $this->text = $text;
        $this->value = $value;
    }

    /**
     * Obtient le texte de l'option de choix.
     *
     * @return string Le texte de l'option de choix.
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * Obtient la valeur associée à l'option de choix.
     *
     * @return string La valeur associée à l'option de choix.
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Renvoie une représentation textuelle de l'option de choix.
     *
     * @return string Le texte de l'option suivi de sa valeur entre parenthèses.
     */
    public function render(): string
    {
        return "{$this->text} ({$this->value})";
    }
}
?>
