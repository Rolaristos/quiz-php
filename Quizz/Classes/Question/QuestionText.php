<?php
namespace Quizz\Classes\Question;

use Quizz\Classes\Answer\Answer;

/**
 * La classe QuestionText représente une question à réponse textuelle dans un quiz.
 * Elle étend la classe abstraite Question.
 */
class QuestionText extends Question
{
    /**
     * Constructeur de la classe QuestionText.
     *
     * @param string        $name     Le nom de la question.
     * @param string        $type     Le type de la question.
     * @param string        $text     Le texte de la question.
     * @param array<Answer> $answers  Les réponses possibles à la question.
     * @param int           $score    Le score attribué à la question.
     */
    public function __construct(string $name, string $type, string $text, array $answers, int $score)
    {
        parent::__construct($name, $type, $text, $answers, $score);
    }

    /**
     * Génère une représentation HTML de la question à réponse textuelle.
     *
     * @return string La représentation HTML de la question.
     */
    public function render(): string
    {
        return "{$this->text}<br><input type='text' name='q{$this->name}'><br>";
    }

    /**
     * Vérifie si la réponse de l'utilisateur est correcte pour la question à réponse textuelle.
     *
     * @param mixed $userAnswer La réponse fournie par l'utilisateur.
     *
     * @return bool True si la réponse est correcte, sinon false.
     */
    public function checkAnswer($userAnswer): bool
    {
        $userAnswerLower = strtolower(str_replace(' ', '', $userAnswer)); // Convertir la réponse de l'utilisateur en minuscules et supprimer les espaces

        foreach ($this->answers as $answer) {
            $correctAnswerLower = strtolower(str_replace(' ', '', $answer->getAnswerText())); // Convertir la réponse correcte en minuscules et supprimer les espaces

            if ($correctAnswerLower === $userAnswerLower) {
                return true;
            }
        }

        return false;
    }

    /**
     * Obtient le score attribué à la question à réponse textuelle.
     *
     * @return int Le score attribué à la question.
     */
    public function getScore(): int
    {
        return $this->score;
    }
}
?>
