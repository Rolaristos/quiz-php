<?php
namespace Quizz\Classes\Question;

use Quizz\Classes\Answer\Answer;

/**
 * La classe abstraite Question représente une question dans un quiz.
 */
abstract class Question
{
    /**
     * @var string Le nom de la question.
     */
    protected $name;

    /**
     * @var string Le type de la question.
     */
    protected $type;

    /**
     * @var string Le texte de la question.
     */
    protected $text;

    /**
     * @var array<Answer> Les réponses possibles à la question.
     */
    protected $answers;

    /**
     * @var int Le score attribué à la question.
     */
    protected $score;

    /**
     * Constructeur de la classe Question.
     *
     * @param string $name     Le nom de la question.
     * @param string $type     Le type de la question.
     * @param string $text     Le texte de la question.
     * @param array  $answers  Les réponses possibles à la question.
     * @param int    $score    Le score attribué à la question.
     */
    public function __construct(string $name, string $type, string $text, array $answers, int $score)
    {
        $this->name = $name;
        $this->type = $type;
        $this->text = $text;
        $this->answers = $answers;
        $this->score = $score;
    }

    /**
     * Méthode abstraite pour générer une représentation textuelle de la question.
     *
     * @return string La représentation textuelle de la question.
     */
    abstract public function render(): string;

    /**
     * Vérifie la réponse de l'utilisateur pour la question.
     *
     * @param mixed $userAnswer La réponse fournie par l'utilisateur.
     *
     * @return bool True si la réponse est correcte, sinon false.
     */
    public function answer($userAnswer): bool
    {
        foreach ($this->answers as $answer) {
            if ($answer->evaluate($userAnswer)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Obtient le nom de la question.
     *
     * @return string Le nom de la question.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Obtient les réponses possibles à la question.
     *
     * @return array Les réponses possibles à la question.
     */
    public function getAnswers(): array
    {
        return $this->answers;
    }

    /**
     * Affiche les réponses possibles à la question.
     *
     * @return string Une chaîne de caractères représentant les réponses possibles.
     */
    public function displayAnswers(): string
    {
        $response = [];
        foreach ($this->answers as $answer) {
            $response[] = "Réponse : " . $answer->render();
        }
        return implode('<br>', $response);
    }

    /**
     * Obtient le texte de la question.
     *
     * @return string Le texte de la question.
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * Obtient le type de la question.
     *
     * @return string Le type de la question.
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Obtient le score attribué à la question.
     *
     * @return int Le score attribué à la question.
     */
    public function getScore(): int
    {
        return $this->score;
    }
}
?>
