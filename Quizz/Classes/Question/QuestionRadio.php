<?php
namespace Quizz\Classes\Question;

use Quizz\Classes\Answer\Answer;
use Quizz\Classes\Choice\Choice;

/**
 * La classe QuestionRadio représente une question à choix unique dans un quiz.
 * Elle étend la classe abstraite Question.
 */
class QuestionRadio extends Question
{
    /**
     * @var array<Choice> Les options de choix disponibles pour la question à choix unique.
     */
    private array $choices;

    /**
     * Constructeur de la classe QuestionRadio.
     *
     * @param string $name     Le nom de la question.
     * @param string $type     Le type de la question.
     * @param string $text     Le texte de la question.
     * @param array<Answer>  $answers  Les réponses possibles à la question.
     * @param int    $score    Le score attribué à la question.
     * @param array<Choice>  $choices  Les options de choix disponibles pour la question à choix unique.
     */
    public function __construct(string $name, string $type, string $text, array $answers, int $score, array $choices)
    {
        parent::__construct($name, $type, $text, $answers, $score);
        $this->choices = $choices;
    }

    /**
     * Génère une représentation HTML de la question à choix unique.
     *
     * @return string La représentation HTML de la question.
     */
    public function render(): string
    {
        $html = $this->text . "<br>";
        $i = 0;
        foreach ($this->choices as $c) {
            $i += 1;
            $html .= "<input type='radio' name='q{$this->name}' value='{$c->getValue()}' id='{$this->name}-{$i}'>";
            $html .= "<label for='{$this->name}-{$i}'>{$c->getText()}</label>";
        }
        return $html;
    }

    /**
     * Vérifie si la réponse de l'utilisateur est correcte pour la question à choix unique.
     *
     * @param mixed $userAnswer La réponse fournie par l'utilisateur.
     *
     * @return bool True si la réponse est correcte, sinon false.
     */
    public function checkAnswer($userAnswer): bool
    {
        foreach ($this->answers as $answer) {
            if ($answer->getAnswerId() === (int)$userAnswer) {
                return true;
            }
        }
        return false;
    }

    /**
     * Obtient le score attribué à la question à choix unique.
     *
     * @return int Le score attribué à la question.
     */
    public function getScore(): int
    {
        return $this->score;
    }
}
?>
