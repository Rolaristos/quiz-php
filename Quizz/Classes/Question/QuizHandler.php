<?php
namespace Quizz\Classes\Question;

/**
 * La classe QuizHandler gère le traitement et l'affichage d'un quiz.
 */
class QuizHandler
{
    /**
     * @var array<Question> Les questions du quiz.
     */
    private $questions;

    /**
     * @var string L'action associée au formulaire du quiz.
     */
    private $action;

    /**
     * Constructeur de la classe QuizHandler.
     *
     * @param array<Question> $questions Les questions du quiz.
     * @param string          $act       L'action associée au formulaire du quiz.
     */
    public function __construct(array $questions, $act)
    {
        $this->questions = $questions;
        $this->action = $act;
    }

    /**
     * Génère le formulaire HTML pour le quiz.
     *
     * @return string Le formulaire HTML.
     */
    public function renderForm(): string
    {
        $html = "<form method='POST' action='{$this->action}'>";
        foreach ($this->questions as $q) {
            $html .= "<li class='question'>";
            $html .= $q->render() . "</li>";
        }
        $html .= "<br><input type='submit' name='submitQuiz' value='Répondre'></form>";

        return $html;
    }

    /**
     * Traite les résultats du quiz.
     *
     * @return array Un tableau contenant les statistiques des résultats du quiz.
     */
    public function processResults(): array
    {
        $results = [
            'questionTotal'   => 0,
            'questionCorrect' => 0,
            'scoreTotal'      => 0,
            'scoreCorrect'    => 0,
        ];

        foreach ($this->questions as $q) {
            $results['questionTotal'] += 1;
            $userAnswer = $_POST['q' . $q->getName()] ?? null;
            if ($q->checkAnswer($userAnswer)) {
                $results['questionCorrect'] += 1;
                $results['scoreCorrect'] += $q->getScore();
            }

            $results['scoreTotal'] += $q->getScore();
        }

        return $results;
    }
}
?>
