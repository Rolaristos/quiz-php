<?php
namespace Quizz\Classes;

require_once __DIR__ . '/Autoloader.php';
use Quizz\Classes\Autoloader;

/**
 * La classe Quizz représente un quiz composé de questions.
 */
class Quizz
{
    /**
     * @var int L'identifiant du quiz.
     */
    protected $id;

    /**
     * @var string Le nom du quiz.
     */
    protected $name;

    /**
     * @var array<Question> Les questions du quiz.
     */
    protected $questions;

    /**
     * @var string La description du quiz.
     */
    protected $description;

    /**
     * @var int La difficulté du quiz.
     */
    protected $difficulte;

    /**
     * Constructeur de la classe Quizz.
     *
     * @param int           $id           L'identifiant du quiz.
     * @param string        $name         Le nom du quiz.
     * @param array<Question> $question Les questions du quiz.
     * @param string        $description  La description du quiz.
     * @param int           $difficulte   La difficulté du quiz.
     */
    public function __construct(int $id, string $name, array $lesQuestion, string $description, int $difficulte)
    {
        $this->id = $id;
        $this->name = $name;
        $this->questions = $lesQuestion;
        $this->description = $description;
        $this->difficulte = $difficulte;
    }

    /**
     * Obtient l'identifiant du quiz.
     *
     * @return int L'identifiant du quiz.
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Obtient le nom du quiz.
     *
     * @return string Le nom du quiz.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Obtient les questions du quiz.
     *
     * @return array<Question> Les questions du quiz.
     */
    public function getQuestions(): array
    {
        return $this->questions;
    }

    /**
     * Obtient la description du quiz.
     *
     * @return string La description du quiz.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Obtient la difficulté du quiz.
     *
     * @return int La difficulté du quiz.
     */
    public function getDifficulte(): int
    {
        return $this->difficulte;
    }

    /**
     * Définit l'identifiant du quiz.
     *
     * @param int $id L'identifiant du quiz.
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * Définit le nom du quiz.
     *
     * @param string $name Le nom du quiz.
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Définit les questions du quiz.
     *
     * @param array<Question> $questions Les questions du quiz.
     */
    public function setQuestions(array $questions): void
    {
        $this->questions = $questions;
    }

    /**
     * Définit la description du quiz.
     *
     * @param string $description La description du quiz.
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * Définit la difficulté du quiz.
     *
     * @param int $difficulte La difficulté du quiz.
     */
    public function setDifficulte(int $difficulte): void
    {
        $this->difficulte = $difficulte;
    }

    /**
     * Ajoute une question au quiz.
     *
     * @param Question $quest La question à ajouter.
     */
    public function addQuestion(Question $quest): void
    {
        $this->questions[] = $quest;
    }
}
?>
