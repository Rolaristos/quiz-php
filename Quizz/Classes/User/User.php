<?php
namespace Quizz\Classes\User;

/**
 * La classe User représente un utilisateur du système.
 */
class User
{
    /**
     * @var string Le pseudo de l'utilisateur.
     */
    private string $pseudo;

    /**
     * @var string Le mot de passe de l'utilisateur.
     */
    private string $mdp;

    /**
     * Constructeur de la classe User.
     *
     * @param string $pseudo Le pseudo de l'utilisateur.
     * @param string $mdp    Le mot de passe de l'utilisateur.
     */
    public function __construct(string $pseudo, string $mdp)
    {
        $this->pseudo = $pseudo;
        $this->mdp = $mdp;
    }

    /**
     * Obtient le pseudo de l'utilisateur.
     *
     * @return string Le pseudo de l'utilisateur.
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * Obtient le mot de passe de l'utilisateur.
     *
     * @return string Le mot de passe de l'utilisateur.
     */
    public function getMdp()
    {
        return $this->mdp;
    }
}
?>
