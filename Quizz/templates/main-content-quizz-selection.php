<?php
namespace Quizz\templates;

require_once __DIR__ . '/../Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

require __DIR__ . '/../../db/php/DB_connection.php';
require __DIR__ . '/../../db/php/DBClasses/QuizzTermineDB.php';

use db\php\DBClasses\QuizzTermineDB;

$__QUIZZ_TERMINE_BD__ = new QuizzTermineDB($cnx);

// Récupération des informations sur l'utilisateur depuis la session
$user = unserialize($_SESSION['user']);
$pseudo = $user->getPseudo();
?>

<!-- Div pour l'overlay de chargement -->
<div class="loading-overlay"></div>

<!-- Formulaire pour retourner au menu principal -->
<form action="minami-quizz-menu.php" method="post">
    <input type="hidden" name="animation" value="0">
    <button class="button" type="submit">
    <svg class="svgIcon" viewBox="0 0 384 512">
        <path
        d="M214.6 41.4c-12.5-12.5-32.8-12.5-45.3 0l-160 160c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L160 141.2V448c0 17.7 14.3 32 32 32s32-14.3 32-32V141.2L329.4 246.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3l-160-160z"
        ></path>
    </svg>
    </button>
</form>

<!-- Div pour le contenu défilable -->
<div class="scrollable-content">
    <div class="content-quizz-selection">
        <?php
        // Récupération de la liste de quiz en fonction des paramètres GET
        if (isset($_GET['niveau']) && $_GET['niveau'] == 0) {
            $quizz_list = $__QUIZZ__->getRandomQuizzList(5);
        } else {
            if (isset($_GET['niveau'])) {
                $quizz_list = $__QUIZZ__->getQuizzByDifficult($_GET['niveau']);
            } else {
                if (isset($_GET['finished']) && $_GET['finished'] === 'true') {
                    $quizz_list = $__QUIZZ_TERMINE_BD__->getAllQuizzFini($pseudo);
                } else {
                    $quizz_list = $__QUIZZ__->getAllQuizz();
                }
            }
        }

        // Affichage de chaque élément de la liste de quiz
        foreach ($quizz_list as $q) {
            $score = $__QUIZZ_TERMINE_BD__->getScoreByPseudoAndQuizzId($pseudo, $q->getId());
            $statusText = $score !== null ? "Score : $score" : 'Pas encore tenté';
            $buttonText = $score !== null ? 'Rejouer' : 'Jouer';

            // Affichage du bloc de sélection de quiz
            echo <<<EOL
                <div class="quizz-selection">
                    <div class="quizz-selection-title">
                        <h2>{$q->getName()}</h2>
                    </div>
                    <div class="quizz-selection-description">
                        <p>{$q->getDescription()}</p>
                        <p>---<br/>{$statusText}</p>
                    </div>
                    <div class="quizz-selection-button">
                        <a href="quizz.php?id={$q->getId()}">$buttonText</a>
                    </div>
                </div>
            EOL;
        }
        ?>
    </div>
</div>
