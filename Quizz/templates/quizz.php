<?php
namespace Quizz\templates;
session_start();

if (!isset($_SESSION['user'])) {
    header("Location: /light_novel/templates/login.php");
    exit;
}

require_once __DIR__ . '/../Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

require __DIR__ . '/../Classes/Question/QuizHandler.php';
require __DIR__ . '/../../db/php/DB_connection.php';
require __DIR__ . '/../../db/php/DBClasses/QuestionDB.php';
require __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';
require __DIR__ . '/../../db/php/DBClasses/QuizzTermineDB.php';

use db\php\DBClasses\QuizzTermineDB;
use db\php\DBClasses\QuestionDB;
use db\php\DBClasses\QuizzDB;

// Création des instances nécessaires
$quizzTermineDB = new QuizzTermineDB($cnx);
$__QUESTION__ = new QuestionDB($cnx);
$__QUIZZ__ = new QuizzDB($cnx);

$quizz = $__QUIZZ__->getQuizzById($_GET['id']);
$questions = $quizz->getQuestions();
$difficulte = $quizz->getDifficulte();

// Conversion du niveau de difficulté en texte
if ($difficulte == 1) {
    $difficulte = "Difficulté : Facile";
}
if ($difficulte == 2) {
    $difficulte = "Difficulté : Normal";
}
if ($difficulte == 3) {
    $difficulte = "Difficulté : Difficile";
}
if ($difficulte == 4) {
    $difficulte = "Difficulté : Extrême";
}

// Récupération du numéro du chapitre
$num_chapter = $__QUIZZ__->getChapterById($quizz->getId());

$quizHandler = new \Quizz\Classes\Question\QuizHandler($questions, 'quizz.php?id=' . $_GET['id']);

// Définition du texte du bouton en fonction de la méthode de la requête
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $btnText = "retour";
} else {
    $btnText = "button";
}

?>

<!DOCTYPE html>
<html lang="fr" style="height:100%;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Minami no hanashi - Quizz</title>
    <link rel="icon" href="../../light_novel/static/images/icon/flavicon.svg" type="image/svg+xml">
    <link rel="stylesheet" href="../static/styles/quizz.css">
    <script>
        function positionnerCerclesAleatoirement() {
            const container = document.querySelector('.container');
            const cercles = container.querySelectorAll('.circle-container');

            for (let cercle of cercles) {
                const left = Math.random() * 100 - 50; // Entre -50% et 50%
                const top = Math.random() * 80 + 10;  // Entre 0% et 90%

                cercle.style.left = `${left}%`;
                cercle.style.top = `${top}%`;
            }
        }

        window.addEventListener('DOMContentLoaded', function () {
            document.body.classList.add('loaded');
            positionnerCerclesAleatoirement();
        });
    </script>
</head>

<body style="height:100%; margin:0;">
    <!-- Formulaire de retour -->
    <form class = "back" action="minami-quizz-menu.php" method="post">
        <input type="hidden" name="animation" value="0">
        <button class='<?php echo $btnText;?>' type="submit">
        <svg class="svgIcon" viewBox="0 0 384 512">
            <path
            d="M214.6 41.4c-12.5-12.5-32.8-12.5-45.3 0l-160 160c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L160 141.2V448c0 17.7 14.3 32 32 32s32-14.3 32-32V141.2L329.4 246.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3l-160-160z"
            ></path>
        </svg>
        </button>
    </form>
    <div class="container">
        <!-- Utilisation d'une boucle pour générer les cercles -->
        <?php for ($i = 1; $i <= 500; $i++) { ?>
            <div class="circle-container">
                <div class="circle" style="display:flex;"><img src="../static/images/papillon.png" alt="papillon"></div>
            </div>
        <?php } ?>
    </div>

    <?php
    // Traitement des résultats si le formulaire a été soumis
    $results = $quizHandler->processResults();
    $bestresult = $quizzTermineDB->getMaxScore($_GET['id']);
    if (isset($_POST['submitQuiz'])) {
        $user = unserialize($_SESSION['user']);
        $pseudo = $user->getPseudo();
        $quizzId = $_GET['id'];
    
        $previousScore = $quizzTermineDB->getScoreByPseudoAndQuizzId($pseudo, $quizzId);
    
        if ($previousScore !== null) {
            echo <<<EOL
                <h2></h2>
                <div class='questionCorrect'>
                    <h1>Résultats</h1>
                    <p class='information'>Vous avez déjà terminé ce quizz avec un score de {$previousScore} points.<br/>
                    Cette fois-ci, Vous avez un score de {$results['scoreCorrect']} sur {$results['scoreTotal']}.
                    <br/>Malheureusement, ce nouveau résultat ne sera pas pris en compte.</p>
                    <h2 class='best-score'>Le plus score le plus élevé est de : {$bestresult['max_score']}</h2>
                    <p class='name'>Il est détenu par <strong>{$bestresult['pseudo']}</strong>.<p>
                </div>
                <div></div>
            EOL;
        } elseif ($results['questionCorrect'] == $results['questionTotal']) {
            $score = $results['scoreCorrect'];
            $quizzTermineDB->insertQuizzTermine($pseudo, $quizzId, $score);
            // On rappelle la fonction car l'utilisateur peut avoir le meilleur score.
            $bestresult = $quizzTermineDB->getMaxScore($_GET['id']);
    
            echo <<<EOL
                <div class='mainContainer'>
                    <div class='firstBox'>
                        <div class='containerBoxReponses'>
                            <div class='questionCorrect'>
                                <h1>Résultats</h1>
                                <p>Vous avez {$results['questionCorrect']} questions correctes sur {$results['questionTotal']} questions.</p>
                                <p>Vous avez obtenu {$results['scoreCorrect']} points sur {$results['scoreTotal']} points.</p>
                                <div class='meilleurScore'>
                                    <p class='best-score'>Le plus haut score est de : {$bestresult['max_score']}</p>
                                    <p class='name'>Il est détenu par <strong>{$bestresult['pseudo']}</strong>.<p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='secondBox'>
                        <img class='imageMinamiContente' src='../static/images/quizz-3.png'>
                    </div>
                </div>
               
            EOL;
        } else{
            $score = $results['scoreCorrect'];
            $quizzTermineDB->insertQuizzTermine($pseudo, $quizzId, $score);
            // On rappelle la fonction car l'utilisateur peut avoir le meilleur score.
            $bestresult = $quizzTermineDB->getMaxScore($_GET['id']);
    
            echo <<<EOL
                <div class='mainContainer'>
                    <div class='firstBox'>
                        <div class='containerBoxReponses'>
                            <div class='questionCorrect'>
                                <h1>Résultats</h1>
                                <p>Vous avez {$results['questionCorrect']} questions correctes sur {$results['questionTotal']} questions.</p>
                                <p>Vous avez obtenu {$results['scoreCorrect']} points sur {$results['scoreTotal']} points.</p>
                                <div class='meilleurScore'>
                                    <p class='best-score'>Le plus haut score est de : {$bestresult['max_score']}</p>
                                    <p class='name'>Il est détenu par <strong>{$bestresult['pseudo']}</strong>.<p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='secondBox'>
                        <img class='imageMinamiContente' src='../static/images/quizz-2.png'>
                    </div>
                </div>
               
            EOL; 
        }
    } else {
        // Affichage des informations sur le quizz et formulaire de questions
        echo <<<EOL
            <div class="quizz-info">
                <div class="static-info">
                    <h1>{$quizz->getName()}</h1>
                    <p class="desc-quizz">{$quizz->getDescription()}</p>
                    <p>{$difficulte}</p>
            EOL;
        // Affichage du bouton pour voir le chapitre si disponible
        // Numéro chapitre à -1 par défaut, signifiant que le quiz est associé à aucun chapitre
        if ($num_chapter != -1) {
            echo "<button id='togglePdfButton' onclick='togglePdf()'>Voir chapitre</button>";
        }
        echo <<<EOL
                </div>
                <div id="pdfContainer" class="pdf-contains hidden transition">
                    <div class="pdf-contains">
                        <iframe class="pdf" src="../static/chapters/Chapitre{$num_chapter}.pdf" width="100%" height="600px" style="border: none; transform: scale(1);"></iframe>
                    </div>
                </div>
            </div>
            <div class="questions-content">
                <div class="quizz-questions">
                    <h1>Questions</h1>
                    {$quizHandler->renderForm()}
                </div>
            </div>

            <script>
            // Fonction pour basculer l'affichage du chapitre
            function togglePdf() {
                var pdfContainer = document.getElementById('pdfContainer');
                var togglePdfButton = document.getElementById('togglePdfButton');
        
                if (pdfContainer.classList.contains('hidden')) {
                    pdfContainer.classList.remove('hidden');
                    togglePdfButton.textContent = 'Fermer chapitre';
                } else {
                    pdfContainer.classList.add('hidden');
                    togglePdfButton.textContent = 'Voir chapitre';
                }
            }
            </script>        
            EOL;
    }
    ?>
</body>

</html>