<?php
namespace Quizz\templates;

session_start();

if (!isset($_SESSION['user'])) {
    header("Location: /light_novel/templates/login.php");
    exit;
}

require_once __DIR__ . '/../Classes/Autoloader.php';

use \Quizz\Classes\Autoloader;

Autoloader::register();

require __DIR__ . '/../../db/php/DB_connection.php';
require __DIR__ . '/../../db/php/DBClasses/QuestionDB.php';
require __DIR__ . '/../../db/php/DBClasses/QuizzDB.php';

use db\php\DBClasses\QuestionDB;
use db\php\DBClasses\QuizzDB;

$__QUESTION__ = new QuestionDB($cnx);
$__QUIZZ__ = new QuizzDB($cnx);
?>

<!DOCTYPE html>
<html lang="fr" style="height:100%;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Minami no hanashi</title>
    <link rel="icon" href="../../light_novel/static/images/icon/flavicon.svg" type="image/svg+xml">
    <link rel="stylesheet" href="../../light_novel/static/styles/body.css">
    <!-- <link rel="stylesheet" href="../../light_novel/static/styles/header.css"> -->
    <link rel="stylesheet" href="../static/styles/main-content-quizz-selection.css">
    <link rel="stylesheet" href="../static/styles/animation/animation.css">
    <!-- Simple script créant l'effet visuel des cercles qui "s'envolent" -->
    <script>
        function positionnerCerclesAleatoirement() {
            const container = document.querySelector('.container');
            const cercles = container.querySelectorAll('.circle-container');

            for (let cercle of cercles) {
                const left = Math.random() * 100 - 10; // Entre -10% et 90%
                const top = Math.random() * 80 + 10;  // Entre 10% et 90%

                cercle.style.left = `${left}%`;
                cercle.style.top = `${top}%`;
            }
        }

        window.addEventListener('load', function () {
            document.body.classList.add('loaded');
            positionnerCerclesAleatoirement();
        });
    </script>
</head>

<body style="height:100%; margin:0;">
    <div class="background-filter">
        <div class="container">
            <?php for ($i = 1; $i <= 1000; $i++) { 
                $randomValue = mt_rand(-9, 8);
            ?>
                <div class="circle-container">
                    <div class="circle"></div>
                </div>
            <?php } ?>
        </div>
        <?php
        // require_once '../../light_novel/templates/header.php';
        require_once 'main-content-quizz-selection.php';
        ?>
    </div>
    <script>
        window.addEventListener('load', function () {
            document.body.classList.add('loaded');
        });
    </script>
</body>
</html>
