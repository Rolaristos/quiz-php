<?php
namespace Quizz\templates;
session_start();

// Redirection vers la page de connexion si l'utilisateur n'est pas connecté
if (!isset($_SESSION['user'])) {
    header("Location: /light_novel/templates/login.php");
    exit;
}

// Gestion de l'état de chargement dans la session
if (!isset($_SESSION['loading'])) {
    $_SESSION['loading'] = 'loading';
} else {
    $_SESSION['loading'] = 'not-loading';
}
?>

<!DOCTYPE html>
<html lang="fr" style="height:100%;">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Minami no hanashi</title>
    <link rel="icon" href="../../light_novel/static/images/icon/flavicon.svg" type="image/svg+xml">
    <link rel="stylesheet" href="../../light_novel/static/styles/body.css">
    <link rel="stylesheet" href="../../light_novel/static/styles/header.css">
    <link rel="stylesheet" href="../static/styles/animation/swing.css">
    <link rel="stylesheet" href="../static/styles/loading.css">
    <link rel="stylesheet" href="../static/styles/main-quizz-selection.css">
</head>

<body style="height:100%; margin:0;">
    <!-- Div avec la classe background-filter et l'état de chargement -->
    <div class="background-filter <?php echo $_SESSION['loading']; ?>" id="loading">
        <?php
        require_once '../../light_novel/templates/header.php';
        require_once 'main-quizz-loading.php';
        require_once 'main-content.php';
        ?>
    </div>
    <script src="../static/scripts/loading.js"></script>
</body>
</html>
