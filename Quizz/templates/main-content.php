<?php
    namespace Quizz\templates;
?>
<!-- Les différent groupes de questions -->
<div class="main-content">
    <div class="quizz-content">
        <h1 class="select-quizz">Choisissez votre épreuve</h1>
        <div class="contains">
            <div class="choice-list">
                <a href="minami-quizz.php?niveau=1"><h3 class="choice facile">Niveau facile</h3></a>
                <a href="minami-quizz.php?niveau=2"><h3 class="choice normal">Niveau normal</h3></a>
                <a href="minami-quizz.php?niveau=3"><h3 class="choice difficile">Niveau difficile</h3></a>
                <a href="minami-quizz.php?niveau=4"><h3 class="choice extreme">Niveau extrême</h3></a>
                <a href="minami-quizz.php?niveau=0"><h3 class="choice random">Questions aléatoires</h3></a>
                <a href="minami-quizz.php?finished=true"><h3 class="choice myQuizz">Quiz terminés</h3></a>
                <a href="minami-quizz.php"><h3 class="choice allQuiz">Tous les Quiz</h3></a>
            </div>
        </div>
    </div>
</div>
