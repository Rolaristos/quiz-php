<?php
    namespace Quizz\templates;
?>

<div class="loading-animation">
    <h1 class="title-quizz">Chargement des Quizz</h1>
    <img class="swing-effect" src="../static/images/quizz-3.png" alt="minami-quizz">
    <div class="loading-bar-container">
        <div class="loading-bar" id="loading-bar"></div>
    </div>
</div>